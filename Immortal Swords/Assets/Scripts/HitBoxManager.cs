﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxManager : MonoBehaviour {

    // Set these in the editor
    public PolygonCollider2D bodyHighStand;
    public PolygonCollider2D bodyHighAttack;
    public PolygonCollider2D bodyMediumStand;
    public PolygonCollider2D bodyMediumAttack;
    public PolygonCollider2D bodyLowStand;
    public PolygonCollider2D bodyLowAttack;

    public PolygonCollider2D swordHighStand;
    public PolygonCollider2D swordHighAttack;
    public PolygonCollider2D swordMediumStand;
    public PolygonCollider2D swordMediumAttack;
    public PolygonCollider2D swordLowStand;
    public PolygonCollider2D swordLowAttack;

    // Used for organization
    private PolygonCollider2D[] strikeColliders;
    private PolygonCollider2D[] hurtColliders;

    // Collider on this game object
    private PolygonCollider2D localStrikeCollider;
    private PolygonCollider2D localHurtCollider;

    public enum hitBoxes
    {
        highStance,
        highAttack,
        mediumStance,
        mediumAttack,
        lowStance,
        lowAttack,
        clear // special case to remove all boxes
    }

    // Use this for initialization
    void Start()
    {
        // Set up an array so our script can more easily set up the hit boxes
        hurtColliders = new PolygonCollider2D[] { bodyHighStand, bodyHighAttack,bodyMediumStand, bodyMediumAttack, bodyLowStand, bodyLowAttack };

        //// Create a polygon collider
        //localHurtCollider = gameObject.AddComponent<PolygonCollider2D>();
        //localHurtCollider.isTrigger = true; // Set as a trigger so it doesn't collide with our environment
        //localHurtCollider.pathCount = 0; // Clear auto-generated polygons

        // do same for striek colliders
        strikeColliders = new PolygonCollider2D[] { swordHighStand, swordHighAttack, swordMediumStand, swordMediumAttack, swordLowStand, swordLowAttack };
        
        //localStrikeCollider = gameObject.AddComponent<PolygonCollider2D>();
        //localStrikeCollider.isTrigger = true; 
        //localStrikeCollider.pathCount = 0; 
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Collider hit something!");
    }

    public void setHitBox(hitBoxes val)
    {
        if (val != hitBoxes.clear)
        {
            //localHurtCollider.SetPath(0, hurtColliders[(int)val].GetPath(0));
            //localStrikeCollider.SetPath(0, strikeColliders[(int)val].GetPath(0));
            if ((int)val % 2 == 1)
                GetComponent<Player>().hitboxes[0].SetPath(0, strikeColliders[(int)val].GetPath(0));
            else
                GetComponent<Player>().hitboxes[0].pathCount = 0;
            GetComponent<Player>().hitboxes[1].SetPath(0, hurtColliders[(int)val].GetPath(0));
            return;
        }
        //localHurtCollider.pathCount = 0;
        //localStrikeCollider.pathCount = 0;
        GetComponent<Player>().hitboxes[0].pathCount = 0;
        GetComponent<Player>().hitboxes[1].pathCount = 0;
    }
}
