﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManager : MonoBehaviour {
    public Animator anime;
    public GameObject prefab;
    private GameObject player;
    private Player playerScript;
    private Player aiScript;


    public float close;
    public float mid;
    public float far;
    public GameObject wallRight;
    public GameObject wallLeft;

    public int swordID;
    public int difficulty;



    //Variables for Passive Poke Behave
    private bool poking;
    private int pokes;
    private int totalPokes;
    private float pokeTimer;
    private float pokeWaitTime;

    //Variables for Aggressive Lay In
    private bool layIn;
    private int layInAttacks;
    private int layInTotalAttacks;
    private float layInTimer;
    private float layInTime;
    private float layInWaitTimer;
    private float layInWaitTime;
    private float layInBehaviour;

    //Variables for Defensive avoid
    private bool avoid;
    private float avoidTimer;
    private float avoidTime;
    private float avoidCloseBehaviour;

    private bool attackSwtich;
    


    [SerializeField]
    private float aiTimer;
    private float aiTime;
    private float attackTimer;
    private long playerLightAttack;
    private long playerHeaveyAttack;
    private bool lightAttack;
    private float closeRef;
    private float midRef;
    private float farRef;
    private float stanceTimer;
    private float stanceTime;
    private float flipTimer;


    //values for difficulty
    private float diff_stanceSwitch;
    private float diff_stanceSwitchTime;
    private float diff_switchAgress;
    private int diff_maxLayin;
    private int diff_maxPokes;
    private float diff_pokeWait;
    private float diff_avoidMid;
    private float diff_avoidClose;
    private float diff_attackChance;
    private float diff_flipTime;



    private enum Mood
    {
        Passive,
        Defensive,
        Aggressive,
        None
    };

    private enum Behaviour
    {
        Idle,
        WalkAway,
        WalkTowards,
        DashAway,
        DashTowards,
        Attack,
        AttackLight,
        AttackHeavey,
        None
    };

    [SerializeField] private GameObject aiPlayer;
    [SerializeField] private Mood mood;
    [SerializeField] private Behaviour behaviour;

    void Awake()
    {
        if (GameData.gameData == null)
        {
            GameObject gameObj = new GameObject("GameObject");
            gameObj.AddComponent<GameData>();
            GameData.gameData.menuType = GameData.Menus.Ladder;
        }
        mood = Mood.None;
        behaviour = Behaviour.None;
    }

	// Use this for initialization
	void Start () {

        if (!GameData.gameData.aiVersus)
        {
            gameObject.SetActive(false);
        }
        else
        {
            mood = Mood.Passive;
            behaviour = Behaviour.Idle;
            difficulty = GameData.gameData.aiDifficulty;
        }

        if (difficulty == 2)    //Hard
        {
            diff_stanceSwitch = 0.9f;           //Chance of switching stance when out-stanced       higher -> more diff
            diff_stanceSwitchTime = 0.25f;      //Amount of time before able to switch stance       less ->more diff
            diff_switchAgress = 0.65f;          //Chance of switch behaviour to aggressive          higher -> more diff?
            diff_maxLayin = 9;                  //Amount of max Layin attacks                       higher -> more diff?
            diff_maxPokes = 4;                  //Amount of max Pokes                               higher -> more diff?
            diff_pokeWait = 3f;                 //Chance of waiting in mid                          
            diff_avoidMid = 0.4f;               //Chance of going to mid range                      
            diff_avoidClose = 0.65f;             //Chance of avoiding towards
            diff_attackChance = 0.90f;          //Chance of attacking                               higher -> more diff
            diff_flipTime = 0.0f;
        }
        else if (difficulty == 1)   //Medium
        {
            diff_stanceSwitch = 0.65f;
            diff_stanceSwitchTime = 0.45f;
            diff_switchAgress = 0.60f;
            diff_maxLayin = 7;
            diff_maxPokes = 4;
            diff_pokeWait = 3.25f;
            diff_avoidMid = 0.5f;
            diff_avoidClose = 0.5f;
            diff_attackChance = 0.75f;
            diff_flipTime = 0.5f;
        }
        else    //Easy
        {
            diff_stanceSwitch = 0.40f;
            diff_stanceSwitchTime = 0.75f;
            diff_switchAgress = 0.5f;
            diff_maxLayin = 4;
            diff_maxPokes = 2;
            diff_pokeWait = 3.5f;
            diff_avoidMid = 0.65f;
            diff_avoidClose = 0.4f;
            diff_attackChance = 0.50f;
            diff_flipTime = 1.0f;
        }


        closeRef = close = 1.0f;
        midRef = mid = 6.0f;
        farRef = far = 10.0f;
        //wallRight = 6.0f;
        //wallLeft = -6.0f;

        aiTimer = 0.0f;
        aiTime = Random.Range(6.0f, 15.0f);
        attackTimer = 0.0f;
        attackSwtich = false;
        flipTimer = 0.0f;

        //Poke initialize
        pokeTimer = 0.0f;
        pokes = 0;
        poking = true;

        //LayIn initialize
        layIn = true;
        layInTimer = 0.0f;
        layInAttacks = 0;
        layInTotalAttacks = Random.Range(3, diff_maxLayin);
        layInWaitTimer = 0.0f;
        layInWaitTime = 0.0f;
        layInBehaviour = -1;

        //Avoid initialize
        avoid = true;
        avoidTimer = 0.0f;
        avoidCloseBehaviour = -1;

        stanceTimer = 0.0f;
       
    }
	
	// Update is called once per frame
	void Update () {
        //evaluate enviroment
        aiTimer += Time.deltaTime;
        stanceTimer += Time.deltaTime;
        flipTimer += Time.deltaTime;
        if (aiTimer > aiTime)
        {
            aiTimer = 0.0f;
            aiTime = Random.Range(6.0f, 15.0f);
            attackSwtich = true;
        }
        /*  There are 3 different moods: Passive, Defensive, and Aggressive.
        *   Attack styles:
        *       passive: hover around a mid distance from the play and move in to 'poke' the opponent.
        *       defensive: avoid to regain staminia, if opponent is pushing then ***********************
        *       aggressive: keep distance close and keep attacking the player, keep in mind of staminia.
        *
        */

        //in the right wall
        if (Mathf.Abs(wallRight.transform.position.x - aiPlayer.transform.position.x) <= 1.0f)
        {
            aiScript.evadeLeft();
        }
        //In the left wall
        else if (Mathf.Abs(wallLeft.transform.position.x - aiPlayer.transform.position.x) <= 1.0f)
        {
            aiScript.evadeRight();
        }

        //check flip
        if (flipTimer >= diff_flipTime)
        {
            aiScript.checkDirection();
            flipTimer = 0;
        }


        //check stance
        if (playerScript.stance < aiScript.stance || (playerScript.stance == 1 && aiScript.stance == -1))
        {
            //check random num
            if (stanceTimer > diff_stanceSwitchTime && diff_stanceSwitch > Random.Range(0.0f, 1.0f))
            {
                aiScript.stanceDown();
            }
        }

        //check if heavey or light
        if (!playerScript.blocking)
        {
            if (playerScript.lightAttacking)
            {
                playerLightAttack++;
            }
            else
            {
                playerHeaveyAttack++;
            }
        }

        //check here
        if (aiScript.stamina.fillAmount > 0.25f || playerScript.stamina.fillAmount <= .3)
        {
            //can heavey

            float attak = (playerLightAttack - playerHeaveyAttack) / (playerLightAttack + playerHeaveyAttack + 1) ;
            if (attak < 0) // more heavy attaks
            {
                if ((0.75f + (attak * -1)) > Random.Range(0.0f, 1.0f))
                {
                    lightAttack = true;
                }
                else
                {
                    lightAttack = false;
                }
                    
            }
            else //more light attack or same balance
            {
                if ((0.75f + attak) > Random.Range(0.0f, 1.0f))
                {
                    lightAttack = false;
                }
                else
                {
                    lightAttack = true;
                }
            }
        }
        else
        {
            lightAttack = true;
        }



        if (aiScript.stamina.fillAmount < playerScript.stamina.fillAmount && aiScript.stamina.fillAmount < 0.1f)
        {
            //if stamin low then avoid till stamin higher
            mood = Mood.Defensive;
            avoid = true;
            avoidTimer = 0.0f;
            avoidCloseBehaviour = -1;
        }

        if (aiScript.stamina.fillAmount > 0.8f)
        {
            if (attackSwtich && Random.Range(0.0f, 1.0f) > diff_switchAgress)
            {
                attackSwtich = false;
                mood = Mood.Aggressive;
                layIn = true;
                layInTimer = 0.0f;
                layInAttacks = 0;
                layInTotalAttacks = Random.Range(3, diff_maxLayin);
                layInWaitTimer = 0.0f;
                layInWaitTime = 0.0f;
                layInBehaviour = -1;
            }
            else if (attackSwtich)
            {
                attackSwtich = false;
                mood = Mood.Passive;
                pokeTimer = 0.0f;
                pokes = 0;
                poking = true;
            }
        }

        switch(mood)
        {
            //try to poke
            case Mood.Passive:
          
                //if distance is far range
                if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= far )//- 0.5f)
                {
                    
                    behaviour = Behaviour.WalkTowards;  //Move to mid (towards)
                    mid = midRef - 0.75f;                         //Update mid distance to less than default
                }
                //if distance is mid range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= mid)
                {
                    //Update the mid distance to default
                    pokeTimer += Time.deltaTime;
                    if (pokeTimer > pokeWaitTime)   //if been waiting in mid for the waitTime then go clos eto poke
                    {
                        behaviour = Behaviour.WalkTowards;
                        pokes = 0;
                        totalPokes = Random.Range(1, diff_maxPokes);
                        mid = Random.Range(midRef - 0.55f, midRef);
                    }
                    else
                    {
                        behaviour = Behaviour.Idle;
                    }
                }
                //if distance is close range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= close)
                {
                    if (pokes > totalPokes)
                    {
                        behaviour = Behaviour.WalkAway;
                        mid = Random.Range(midRef, midRef + 1.75f);                            
                    }
                    else
                    {
                        behaviour = Behaviour.Attack;
                        pokeTimer = 0.0f;
                        pokeWaitTime = Random.Range(1.5f, diff_pokeWait);
                    }
                }
                else //to close
                {
                    //move to mid (away)
                    behaviour = Behaviour.WalkAway;
                }

                break;


            //Avoid and gain stamin 
            case Mood.Defensive:


                //if distance is far range
                if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= far)
                {
                    far = farRef;
                    //Stay in far -> check for next action
                    behaviour = Behaviour.Idle;
                    mid = Random.Range(midRef + 1.75f, midRef + 2.75f);
                    avoidCloseBehaviour = -1;
                }
                //if distance is mid range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= mid)
                {
                    mid = midRef + 1.75f;
                    //Move to far (away)
                    behaviour = Behaviour.WalkAway;
                    far = Random.Range(farRef, farRef + 2.0f);
                    if (avoidCloseBehaviour < 0)
                    {
                        avoidCloseBehaviour = Random.Range(0.0f, 1.0f);
                    }
                    if (avoidCloseBehaviour > diff_avoidMid)
                    {
                        behaviour = Behaviour.WalkAway;
                        avoidCloseBehaviour = -1;
                    }
                    else
                    {
                        behaviour = Behaviour.Idle;
                        avoidCloseBehaviour = -1;
                    }
                }
                //if distance is close range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= close)
                {
                    //either back up or dash towards to pass
                    if (avoidCloseBehaviour < 0)
                    {
                        avoidCloseBehaviour = Random.Range(0.0f, 1.0f);
                    }
                    if (avoidCloseBehaviour > diff_avoidClose)
                    {
                        behaviour = Behaviour.DashAway;
                        avoidCloseBehaviour = -1;
                    }
                    else
                    {
                        behaviour = Behaviour.DashTowards;
                        avoidCloseBehaviour = -1;
                    }
                }
                else //to close
                {
                    //move to mid (away)
                    behaviour = Behaviour.WalkAway;
                }


                break;


            //Lay in (close)
            case Mood.Aggressive:

                //if distance is far range
                if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= far)
                {
                    behaviour = Behaviour.WalkTowards;  //Move to mid (towards)
                }

                //if distance is mid range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= mid)
                {
                    layInTimer += Time.deltaTime;
                    if (layInWaitTimer < layInWaitTime)
                    {
                        behaviour = Behaviour.Idle;
                        layInWaitTimer += Time.deltaTime;
                        mid = Random.Range(midRef - 0.55f, midRef);
                    }
                    else
                    {
                        behaviour = Behaviour.WalkTowards;
                        layInBehaviour = 0;
                    }
                }
                //if distance is close range
                else if (Mathf.Abs(player.transform.position.x - aiPlayer.transform.position.x) >= close)
                {
                    //move to mid (away)
                    if (layInAttacks <= layInTotalAttacks) //attack
                    {
                        layInTime = Random.Range(2.5f, 5.0f);
                        behaviour = Behaviour.Attack;

                    }
                    else
                    {
                        layInTimer += Time.deltaTime;
                        if (layInTimer > layInTime)
                        {
                            layInAttacks = 0;
                            layInTotalAttacks = Random.Range(3, diff_maxLayin);
                            layInTimer = 0;
                        }
                        else
                        {
                            //wait and block with possible move to mid real quick
                                
                            if (layInBehaviour < 0) //get a new wait behaviour
                            {
                                layInBehaviour = Random.Range(0.0f, 1.0f);
                                layInWaitTimer = 0.0f;
                                layInWaitTime = Random.Range(0.25f, 2.0f);
                                mid = Random.Range(midRef, midRef + 1.75f);
                            }

                            if (layInBehaviour > 0.66f)   //wait and block
                            {
                                behaviour = Behaviour.Idle;
                                layInWaitTimer += Time.deltaTime;
                                Debug.Log("wait and block");
                                if (layInWaitTimer > layInWaitTime)
                                {
                                    layInBehaviour = -1;
                                }
                            }
                            else if (layInBehaviour < 0.25f)    //Dash towards opponent
                            {
                                behaviour = Behaviour.DashTowards;
                                Debug.Log("Dash");
                                if (aiScript.stamina.fillAmount > 0.5f)
                                {
                                    layInAttacks = 0;
                                    layInTotalAttacks = Random.Range(3, diff_maxLayin);
                                }
                                else
                                {
                                    layInBehaviour = -1;
                                }
                            }
                            else                    //Move to mid
                            {
                                Debug.Log("Move to mid");
                                behaviour = Behaviour.DashAway;
                            }                            

                        }
                    }

                }
                else //to close
                {
                    //move to mid (away)
                    behaviour = Behaviour.WalkAway;
                }

                break;

            case Mood.None:

                break;
        }


        //execute behavior

        switch (behaviour)
        {
            case Behaviour.Idle:

                break;

            case Behaviour.WalkAway:
                if (aiScript.facingLeft)
                {
                    aiScript.right();
                }
                else {
                    aiScript.left();
                }
                break;

            case Behaviour.WalkTowards:
                if (aiScript.facingLeft)
                {
                    aiScript.left();
                }
                else {
                    aiScript.right();
                }
                break;

            case Behaviour.DashAway:
                if (aiScript.facingLeft)
                {
                    aiScript.evadeRight();
                }
                else {
                    aiScript.evadeLeft();
                }
                break;

            case Behaviour.DashTowards:
                if (aiScript.facingLeft)
                {
                    aiScript.evadeLeft();
                }
                else {
                    aiScript.evadeRight();
                }
                break;

            case Behaviour.Attack:
                attackTimer += Time.deltaTime;
                if (attackTimer > 0.5f && Random.Range(0.0f, 1.0f) > diff_attackChance)
                {
                    if (lightAttack)
                    {
                        aiScript.LightAttack();
                    }
                    else
                    {
                        aiScript.HeavyAttack();
                    }
                    
                    attackTimer = 0.0f;
                    if (mood == Mood.Passive && poking)
                    {
                        pokes++;
                    }
                    if (mood == Mood.Aggressive && layIn)
                    {
                        layInAttacks++;
                    }
                }

                break;

            case Behaviour.None:

                break;

        }

    }

    public GameObject createAIPlayer()
    {
        //aiPlayer = new GameObject("AIPlayer");
        aiPlayer = GameObject.Instantiate(prefab);
        aiPlayer.name = "Player AI";

        SpriteRenderer aiRenderer = aiPlayer.GetComponent<SpriteRenderer>();

        aiRenderer.color = Color.gray;

        aiPlayer.transform.Translate(new Vector3(8.0f, 0.0f, 0.0f));
        aiScript = aiPlayer.GetComponent<Player>();
        aiPlayer.transform.Translate(new Vector3(8.0f, 0.0f, 0.0f));
        return aiPlayer;
    }

    public void storeRefs(GameObject playerObj)
    {
        player = playerObj;
        playerScript = playerObj.GetComponent<Player>();
    }
}
