﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Image[] Score;
    public Image[] Score_3;
    public Image[] Score_5;
    public Image[] Score_9;

    public int rounds;
    int winCondition;

    public GameObject prefab;
    public GameObject aiManager;
    public Image staminia1;
    public Image staminia2;
    public BoxCollider sword1;
    public BoxCollider body1;
    public BoxCollider sword2;
    public BoxCollider body2;
    public GameObject mainCameraController;
    public GameObject ladderController;

    public GameObject victory;

    KeyCode left = KeyCode.LeftArrow;
    KeyCode right = KeyCode.RightArrow;

    private float inputDelayTimer;
    private float inputDelay;


    private float inputDelayTimer2;
    private float inputDelay2;

    public GameObject p1;
    public GameObject p2;
    float stamIncreTime;//when to increment stamina
    float time;
    float time2;
    float staminaIncre;
    Player method;
    Player method2;

    
    int p1Wins;
    int p2Wins;

    private Player p1Script;
    private Player p2Script;

    bool play;
    //***********************dashing*******************************
    bool axis_hit_Right;
    bool axis_hit_Left;

    int tap_right;
    int tap_left;

    float inputDelayLeft;
    float inputDelayLeft_MaxTime;

    float inputDelayRight;
    float inputDelayRight_MaxTime;

    //player 2
    bool axis_hit_Right2;
    bool axis_hit_Left2;

    int tap_right2;
    int tap_left2;

    float inputDelayLeft2;
    float inputDelayLeft_MaxTime2;

    float inputDelayRight2;
    float inputDelayRight_MaxTime2;

    //booleans for dashing
    bool p1_dashing;
    Vector3 goal;



    //Used for testing 
    void Awake()
    {
        if (GameData.gameData == null)
        {
            GameObject gameObj = new GameObject("GameObject");
            gameObj.AddComponent<GameData>();
            GameData.gameData.menuType = GameData.Menus.Versus;
        }

    }


    void Start () {

        //Create both players
        p1 = GameObject.Instantiate(prefab);
        p1.name = "Player1";
        SpriteRenderer p1Renderer = p1.GetComponent<SpriteRenderer>();
        //p1Renderer.color = Color.red;
        p1Script = p1.GetComponent<Player>();
        p1Script.stamina = staminia1;
        p1Script.gameManager = gameObject;

        if (GameData.gameData.aiVersus)
        {
            p2 = aiManager.GetComponent<AIManager>().createAIPlayer();
            p2.transform.localScale = new Vector3(-1.75f, 1.75f, 1.75f);
            p2Script = p2.GetComponent<Player>();
            p2Script.opponent = p1;
            p2Script.stamina = staminia2;
            p2Script.gameManager = gameObject;
            p1Script.opponent = p2;
            p2Script.hitboxes.Initialize();
            p1Script.hitboxes.Initialize();
            p2Script.hitboxes = p2.GetComponentsInChildren<PolygonCollider2D>();
            aiManager.GetComponent<AIManager>().storeRefs(p1);
            p2Script.facingLeft = true;
        }
        else
        {
            p2 = GameObject.Instantiate(prefab);
            p2.name = "Player2";
            
            SpriteRenderer p2Renderer = p2.GetComponent<SpriteRenderer>();
            //p2Renderer.flipX = true;
            p2.transform.localScale = new Vector3(-1.75f, 1.75f, 1.75f);
            p2Renderer.color = Color.gray;
            p2.transform.Translate(new Vector3(8.0f, 0.0f, 0.0f));
            p2Script = p2.GetComponent<Player>();
            p2Script.opponent = p1;
            p2Script.facingLeft = true;
        }

        p2Script.stamina = staminia2;
        p2Script.gameManager = gameObject;
        p1Script.opponent = p2;
        p2Script.hitboxes.Initialize();
        p1Script.hitboxes.Initialize();
        p2Script.hitboxes = p2.GetComponentsInChildren<PolygonCollider2D>();
        p1Script.hitboxes = p1.GetComponentsInChildren<PolygonCollider2D>();
        /////////////////////////

        //Add cameraController script to camera and set players
        mainCameraController.AddComponent<CameraControler>();
        mainCameraController.GetComponent<CameraControler>().player = p1;
        mainCameraController.GetComponent<CameraControler>().player2 = p2;

        //stamina increment val
        staminaIncre = 0.010f;
        //time for stamina
        stamIncreTime = 0.10f;
        time = stamIncreTime;
        time2 = stamIncreTime;

        //input delay for P1
        inputDelayTimer = 0.0f;
        inputDelay = 0.30f;

        //input delay for P2
        inputDelayTimer2 = 0.0f;
        inputDelay2 = 0.30f;

        //player methods
        method = (Player)p1.GetComponent(typeof(Player));
        method2 = (Player)p2.GetComponent(typeof(Player));

        //player ids
        method.id = 1;
        method2.id = 2;

        //victory conditions
        
        p1Wins = 0;
        p2Wins = 0;


        play = true;

        //dashing variables******************************************************************
        p1_dashing = false;


        axis_hit_Left= false;
        axis_hit_Right = false;

        tap_left = 0;
        tap_right = 0;

        inputDelayLeft = 0.0f;
        inputDelayLeft_MaxTime = 1.0f;

        inputDelayRight = 0.0f;
        inputDelayRight_MaxTime = 0.25f;

        axis_hit_Right2 = false;
        axis_hit_Left2 = false;

        tap_right2 = 0;
        tap_left2 = 0;

        inputDelayLeft2 = 0;
        inputDelayLeft_MaxTime2 = 0.25f;

        inputDelayRight2 = 0;
        inputDelayRight_MaxTime2 = 0.5f;
        //*********************************************************************************************

        //Number of rounds and victory conditions
        if (GameData.gameData.roundNum == 3)
        {
            Score = Score_3;
            winCondition = 2;
            for (int i = 0; i < Score.Length; i++)
                Score[i].GetComponent<Image>().enabled = true;
        }
        else if (GameData.gameData.roundNum == 5)
        {
            Score = Score_5;
            winCondition = 3;
            for (int i = 0; i < Score.Length; i++)
                Score[i].GetComponent<Image>().enabled = true;
        }
        else
        {
            Score = Score_9;
            winCondition = 5;
            for (int i = 0; i < Score.Length; i++)
                Score[i].GetComponent<Image>().enabled = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        //stamina count down
        time -= Time.deltaTime;
        time2 -= Time.deltaTime;

        //input delay
        inputDelayTimer += Time.deltaTime;
        inputDelayTimer2 += Time.deltaTime;



        //*******************************For Dashing*********************************************
        //left input
        if (tap_left > 0)
            inputDelayLeft += Time.deltaTime;

        if (inputDelayLeft > inputDelayLeft_MaxTime)
        {
            inputDelayLeft = 0.0f;
            tap_left = 0;
        }
        //right input
        if (tap_right > 0)
            inputDelayRight += Time.deltaTime;

        if (inputDelayRight > inputDelayRight_MaxTime)
        {
            inputDelayRight = 0.0f;
            tap_right = 0;
        }

        //left input
        if (tap_left2 > 0)
            inputDelayLeft2 += Time.deltaTime;

        if (inputDelayLeft2 > inputDelayLeft_MaxTime2)
        {
            inputDelayLeft2 = 0.0f;
            tap_left2 = 0;
        }
        //right input
        if (tap_right2 > 0)
            inputDelayRight2 += Time.deltaTime;

        if (inputDelayRight2 > inputDelayRight_MaxTime2)
        {
            inputDelayRight2 = 0.0f;
            tap_right2 = 0;
        }



        //controls
        if (play)
        {
            //player 1 controls***************************************************************************************


            if ((Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("A_1")) && inputDelayTimer >= inputDelay)//light attack
            {
                if (p1 != null)
                {
                    method.LightAttack();
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetKeyDown(KeyCode.Q) || Input.GetButtonDown("B_1")) && inputDelayTimer >= inputDelay)//heavy attack
            {
                if (p1 != null)
                {
                    method.HeavyAttack();
                }
                inputDelayTimer = 0.0f;
            }
            else if ( (Input.GetKey(KeyCode.D) || Input.GetAxis("L_XAxis_1") > 0.5f && inputDelayTimer >= inputDelay) && !p1_dashing)//move right
            {
                tap_left = 0;
                //adds tap
                if (axis_hit_Right == false)
                {
                    axis_hit_Right = true;
                    tap_right++;
                    // Debug.Log("adds Tap: " + taps);
                }
                //evades if tap twice within time else simply moves
                if ((tap_right >= 2) && (inputDelayRight <= inputDelayRight_MaxTime))
                {
                    //Debug.Log("evade");
                    method.evadeRight();
                    //p1_dashing = true;

                    //float y = p1.transform.position.y;
                    //float z = p1.transform.position.z;
                    //float x = p1.transform.position.x + 3.0f;
                    //goal = new Vector3(x, y, z);
                }
                else
                {
                    method.right();
                }
                inputDelayTimer = 0.0f;
                //if my opponent is to my right, then switch direction
                if((p2.transform.position.x > p1.transform.position.x))
                    method.checkDirection();

            }
            else if ( (Input.GetKey(KeyCode.A) || Input.GetAxis("L_XAxis_1") < -0.5f && inputDelayTimer >= inputDelay) && !p1_dashing)//move left
            {
                tap_right = 0;
                //adds tap
                if (axis_hit_Left == false)
                {
                    axis_hit_Left = true;
                    tap_left++;
                   // Debug.Log("adds Tap: " + taps);
                }
                //evades if tap twice within time else simply moves
                if ((tap_left >= 2) && (inputDelayLeft <= inputDelayLeft_MaxTime))
                {
                    //Debug.Log("evade");
                    method.evadeLeft();
                    //Debug.Log("evade");
                    //p1_dashing = true;
                    //float y = p1.transform.position.y;
                    //float z = p1.transform.position.z;
                    //float x = p1.transform.position.x - 3.0f;
                    //goal = new Vector3(x, y, z);
                }
                else
                {
                    method.left();
                }
                inputDelayTimer = 0.0f;
                //if my opponent is to my left, then switch direction
                if ((p2.transform.position.x < p1.transform.position.x))
                    method.checkDirection();
            }
            else if ((Input.GetKeyDown(KeyCode.W) || Input.GetButtonDown("RB_1")) && inputDelayTimer >= inputDelay)
            {
                method.stanceUp();
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetKeyDown(KeyCode.S) || Input.GetButtonDown("LB_1")) && inputDelayTimer >= inputDelay)
            {
                method.stanceDown();
                inputDelayTimer = 0.0f;
            }
            else
            {
                //player 1 velocity
                var v = p1.GetComponent<Rigidbody2D>().velocity;
                v.x = 0.0f;
                v.y = 0.0f;
                p1.GetComponent<Rigidbody2D>().velocity = v;

                //players stamina
                if (time < 0)
                {
                    time = stamIncreTime;
                    method.updateBar(staminaIncre);
                }

                //player 1 block
                method.block();

                //axis
                axis_hit_Left = false;
                axis_hit_Right = false;

                //dashing action
                //if(p1_dashing)
                //{
                //    float y = p1.transform.position.y;
                //    float z = p1.transform.position.z;
                //    float x = p1.transform.position.x - 0.5f;
                //    p1.transform.position = new Vector3(x, y, z);
                //    Debug.Log(p1.transform.position);
                //    Debug.Log("goal is " + goal);
                //    if (goal.x == p1.transform.position.x)
                //        p1_dashing = false;
                //}
            }

            //player 2 controls*****************************************************************************************************

            if (Input.GetKey(right) || Input.GetAxis("L_XAxis_2") > 0.5f && inputDelayTimer2 >= inputDelay2)//move right
            {
                tap_left2 = 0;
                //adds tap
                if (axis_hit_Right2 == false)
                {
                    axis_hit_Right2 = true;
                    tap_right2++;
                }
                //evades if tap twice within time else simply moves
                if ((tap_right2 >= 2) && (inputDelayRight2 <= inputDelayRight_MaxTime2))
                {
                    method2.evadeRight();
                }
                else
                {
                    method2.right();
                }
                inputDelayTimer2 = 0.0f;
                //if my opponent is to my right, then switch direction
                if ((p1.transform.position.x > p2.transform.position.x))
                    method2.checkDirection();
            }
            else if (Input.GetKey(left) || Input.GetAxis("L_XAxis_2") < -0.5f && inputDelayTimer2 >= inputDelay2)//move left
            {
                tap_right2 = 0;
                //adds tap
                if (axis_hit_Left2 == false)
                {
                    axis_hit_Left2 = true;
                    tap_left2++;
                    // Debug.Log("adds Tap: " + taps);
                }
                //evades if tap twice within time else simply moves
                if ((tap_left2 >= 2) && (inputDelayLeft2 <= inputDelayLeft_MaxTime2))
                {
                    //Debug.Log("evade");
                    method2.evadeLeft();
                }
                else
                {
                    method2.left();
                }
                inputDelayTimer2 = 0.0f;
                //if my opponent is to my left, then switch direction
                if ((p1.transform.position.x < p2.transform.position.x))
                    method2.checkDirection();
            }
            else if ((Input.GetKeyDown(KeyCode.PageDown) || Input.GetButtonDown("A_2")) && inputDelayTimer2 >= inputDelay2)//light attack
            {
                if (p2 != null)
                {
                    method2.LightAttack();
                }
                inputDelayTimer2 = 0.0f;
            }
            else if ((Input.GetKeyDown(KeyCode.PageUp) || Input.GetButtonDown("B_2")) && inputDelayTimer2 >= inputDelay2)//heavy attack
            {
                if (p2 != null)
                {
                    method2.HeavyAttack();
                }
                inputDelayTimer2 = 0.0f;
            }
            else if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetButtonDown("RB_2")) && inputDelayTimer2 >= inputDelay2)
            {
                method2.stanceUp();
                inputDelayTimer2 = 0.0f;
            }
            else if ((Input.GetKeyDown(KeyCode.DownArrow) || Input.GetButtonDown("LB_2")) && inputDelayTimer2 >= inputDelay2)
            {
                method2.stanceDown();
                inputDelayTimer2 = 0.0f;
            }
            //resets velocities, increase stamina, and auto-block players while no is given
            else
            {


                //players stamina
                if (time2 < 0)
                {
                    time2 = stamIncreTime;
                    method2.updateBar(staminaIncre);
                }

                //player 2 velocity
                var v = p2.GetComponent<Rigidbody2D>().velocity;
                v.x = 0.0f;
                v.y = 0.0f;
                p2.GetComponent<Rigidbody2D>().velocity = v;

                //player 2 block
                method2.block();

                //axis
                axis_hit_Left2 = false;
                axis_hit_Right2 = false;
            }
        }
    }

    //called by player
    public void addWin(int player)
    {
        Debug.Log("player id " + player);
        if (player == 1)
        {
            p1Wins++;
            Score[ p1Wins - 1 ].GetComponent<Image>().color = Color.red;

            Debug.Log("wins " + p1Wins);
        }
        else
        {
            p2Wins++;
            Score[Score.Length - p2Wins].GetComponent<Image>().color = Color.blue;
        }

        nextRound();
    }

    void nextRound()
    {
        p1.transform.position = new Vector3(-4.0f, -2.5f, 0.0f);
        method.stamina.fillAmount = 1;
        method.staggered = false;

        p2.transform.position = new Vector3(4.0f, -2.5f, 0.0f);
        method2.stamina.fillAmount = 1;
        method2.staggered = false;

        if (p1Wins >= winCondition || p2Wins >= winCondition)
        {
            //call victory menu
            victory.SetActive(true);
            play = false;
            if (GameData.gameData.menuType == GameData.Menus.Ladder)
            {
                if (p1Wins > p2Wins)
                {
                    ladderController.GetComponent<LadderController>().updateLadder();
                }
                else
                {
                    ladderController.GetComponent<LadderController>().retryMenu.SetActive(true);
                }
            }
        }

    }
}


