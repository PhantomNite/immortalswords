﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SwordMenuManager : MonoBehaviour {
    public GameObject startMenu;
    public GameObject roundsCursor;
    public GameObject opponentCursor;

    //Round widgets
    public Text roundsText;
    public GameObject roundsLeftArrow;
    public GameObject roundsRightArrow;


    //Opponent Widgets
    public Text opponentText;
    public GameObject opponentLeftArrow;
    public GameObject opponentRightArrow;

    public GameObject swordProfile0P1;
    public GameObject swordProfile1P1;
    public GameObject swordProfile2P1;
    public GameObject swordProfile3P1;

    public GameObject swordProfile0P2;
    public GameObject swordProfile1P2;
    public GameObject swordProfile2P2;
    public GameObject swordProfile3P2;

    public Text swordInfoP1;
    public Text swordInfoP2;

    //Sword selectors cursors
    public GameObject sword1Cursor;
    public GameObject sword2Cursor;
    public GameObject sword3Cursor;
    public GameObject sword4Cursor;
    public GameObject sword5Cursor;
    public GameObject sword6Cursor;
    public GameObject sword7Cursor;
    public GameObject sword8Cursor;
    public GameObject sword9Cursor;
    public GameObject sword10Cursor;
    public GameObject sword11Cursor;
    public GameObject sword12Cursor;
    public GameObject sword13Cursor;
    public GameObject sword14Cursor;
    public GameObject sword15Cursor;
    public GameObject sword16Cursor;
    //Sword selector cursors 
    public GameObject sword1Cursor2;
    public GameObject sword2Cursor2;
    public GameObject sword3Cursor2;
    public GameObject sword4Cursor2;
    public GameObject sword5Cursor2;
    public GameObject sword6Cursor2;
    public GameObject sword7Cursor2;
    public GameObject sword8Cursor2;
    public GameObject sword9Cursor2;
    public GameObject sword10Cursor2;
    public GameObject sword11Cursor2;
    public GameObject sword12Cursor2;
    public GameObject sword13Cursor2;
    public GameObject sword14Cursor2;
    public GameObject sword15Cursor2;
    public GameObject sword16Cursor2;




    private GameObject[] tier1Swords;
    private GameObject[] tier2Swords;
    private GameObject[] tier3Swords;
    private GameObject[] tier4Swords;

    private GameObject[] tier1Swords2;
    private GameObject[] tier2Swords2;
    private GameObject[] tier3Swords2;
    private GameObject[] tier4Swords2;

    private string[] swordNames;

    private int[] swords;

    private bool p1Ready;
    private bool p2Ready;
    private bool leftKeyP1;
    private bool rightKeyP1;
    private bool leftKeyP2;
    private bool rightKeyP2;
    private bool secondPlayer;
    private bool ladderBool;

    private int leftKeyNumP1;
    private int rightKeyNumP1;
    private int leftKeyNumP2;
    private int rightKeyNumP2;
    private int optionNum;
    private int optionNum2;
    private int maxOptionNum;
    private int maxOptionNum2;
    private int swordIndex;
    private int swordIndex2;
    private int swordIndexP1;
    private int swordIndexP2;
    private int swordTierIndex;
    private int swordTierIndex2;
    

    private float inputDelayTimer;
    private float inputDelay;
    private float inputDelayTimer2;
    private float inputDelay2;

    void Awake()
    {
        //for testing!!!!!!!!
        if (GameData.gameData == null)
        {
            GameObject gameObj = new GameObject("GameObject");
            gameObj.AddComponent<GameData>();
            GameData.gameData.menuType = GameData.Menus.Versus;
        }
        if (MenuMusicManager.menuMusic == null)
        {
            GameObject musicObj = new GameObject("MusicObject");
            musicObj.AddComponent<MenuMusicManager>();
        }
    }

    // Use this for initialization
    void Start () {
        //Set defualt values
        ladderBool = false;
        swordTierIndex = 0;
        swordTierIndex2 = 0;
        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;
        inputDelayTimer2 = 0.0f;
        inputDelay2 = 0.15f;

        //Set up UI default
        startMenu.SetActive(false);
        roundsCursor.SetActive(false);
        opponentCursor.SetActive(false);
        roundsLeftArrow.SetActive(true);
        roundsRightArrow.SetActive(true);
        opponentLeftArrow.SetActive(true);
        opponentRightArrow.SetActive(true);

        
        swords = new int[16];
        swordNames = new string[] 
        { "Sword5", "Sword6", "Sword7", "Sword8",
            "Sword9", "Sword10", "Sword11", "Sword12",
            "Sword13", "Sword14", "Sword15", "Sword16",
            "SwordEnd1", "SwordEnd2", "SwordEnd3", "SwordEnd4"};

        //Create and set up sword cusor arrays for each tier
        tier1Swords = new GameObject[4];
        tier1Swords[0] = sword1Cursor;
        tier1Swords[1] = sword2Cursor;
        tier1Swords[2] = sword3Cursor;
        tier1Swords[3] = sword4Cursor;

        tier2Swords = new GameObject[4];
        tier2Swords[0] = sword5Cursor;
        tier2Swords[1] = sword6Cursor;
        tier2Swords[2] = sword7Cursor;
        tier2Swords[3] = sword8Cursor;

        tier3Swords = new GameObject[4];
        tier3Swords[0] = sword9Cursor;
        tier3Swords[1] = sword10Cursor;
        tier3Swords[2] = sword11Cursor;
        tier3Swords[3] = sword12Cursor;

        tier4Swords = new GameObject[4];
        tier4Swords[0] = sword13Cursor;
        tier4Swords[1] = sword14Cursor;
        tier4Swords[2] = sword15Cursor;
        tier4Swords[3] = sword16Cursor;



        tier1Swords2 = new GameObject[4];
        tier1Swords2[0] = sword1Cursor2;
        tier1Swords2[1] = sword2Cursor2;
        tier1Swords2[2] = sword3Cursor2;
        tier1Swords2[3] = sword4Cursor2;

        tier2Swords2 = new GameObject[4];
        tier2Swords2[0] = sword5Cursor2;
        tier2Swords2[1] = sword6Cursor2;
        tier2Swords2[2] = sword7Cursor2;
        tier2Swords2[3] = sword8Cursor2;

        tier3Swords2 = new GameObject[4];
        tier3Swords2[0] = sword9Cursor2;
        tier3Swords2[1] = sword10Cursor2;
        tier3Swords2[2] = sword11Cursor2;
        tier3Swords2[3] = sword12Cursor2;

        tier4Swords2 = new GameObject[4];
        tier4Swords2[0] = sword13Cursor2;
        tier4Swords2[1] = sword14Cursor2;
        tier4Swords2[2] = sword15Cursor2;
        tier4Swords2[3] = sword16Cursor2;



        for (int i = 0; i < 4; i++)
        {
            tier1Swords[i].SetActive(false);
            tier2Swords[i].SetActive(false);
            tier3Swords[i].SetActive(false);
            tier4Swords[i].SetActive(false);

            tier1Swords2[i].SetActive(false);
            tier2Swords2[i].SetActive(false);
            tier3Swords2[i].SetActive(false);
            tier4Swords2[i].SetActive(false);
        }


        //Check what type of match is going to be played: versus, ladder, training, tutorial

        if (GameData.gameData.menuType == GameData.Menus.Versus) //Versus Mode
        {
            //set player 1 versus defaults
            maxOptionNum = 5;
            optionNum = 4;
            swordIndex = 1;
            //set player 2 versus defaults
            maxOptionNum2 = 3;
            optionNum2 = 0;
            swordIndex2 = 3;

            roundsCursor.SetActive(true);
            

            string[] names = Input.GetJoystickNames();
            if(names.Length == 2) //are there two controllers plugged in?
            {
                p2Ready = false;
                secondPlayer = true;
                tier1Swords2[swordIndex2].SetActive(true);
                opponentText.text = "2 Player";
            }
            else
            {
                p2Ready = true;
                secondPlayer = false;
                opponentText.text = "AI Easy";
            }



            Debug.Log("Versus - Sword Select");
        }
        else if (GameData.gameData.menuType == GameData.Menus.Ladder) // Ladder Mode
        {
            //Set default ladder values
            maxOptionNum = 3;
            optionNum = 0;
            swordIndex = 0;
            tier1Swords[swordIndex].SetActive(true);
            opponentText.text = swordNames[0];
            roundsText.text = "5";

            //Ladder will not have a second player
            ladderBool = true;
            p2Ready = true;
            secondPlayer = false;

            //The rounds and opponent is predefind therefor no need for cursor and arrows
            roundsLeftArrow.SetActive(false);
            roundsRightArrow.SetActive(false);
            opponentLeftArrow.SetActive(false);
            opponentRightArrow.SetActive(false);
            Debug.Log("Ladder - Sword Select");
        }
        else if (GameData.gameData.menuType == GameData.Menus.Training) // Training Mode
        {
            maxOptionNum = 5;
            optionNum = 4;
            swordIndex = 1;
            roundsCursor.SetActive(true);
            p2Ready = true;
            secondPlayer = false;
            Debug.Log("Training - Sword Select");
        }
        else if (GameData.gameData.menuType == GameData.Menus.Tutorial) // Tutorial Mode
        {
            maxOptionNum = 3;
            optionNum = 0;
            swordIndex = 0;
            tier1Swords[swordIndex].SetActive(true);
            Debug.Log("Tutorial - Sword Select");
        }



        
        

    }
	
	// Update is called once per frame
	void Update () {
		if(startMenu.activeInHierarchy) //if both players are ready
        {
            //Debug.Log(swordIndexP1);
            if(Input.GetButtonDown("Start_1") || Input.GetKeyDown(KeyCode.Return))
            {
                MenuMusicManager.menuMusic.StopMusic();
                setGameData();
                SceneManager.LoadScene("Stage");
            }
            else if(Input.GetButtonDown("Back_1") || Input.GetKeyDown(KeyCode.Backspace))
            {
                p1Ready = false;
                if(secondPlayer)
                {
                    p1Ready = false;
                }
                startMenu.SetActive(false);
            }
        }
        else
        {
            inputDelayTimer += Time.deltaTime;
            selectSwordP1();

            if (secondPlayer)
            {
                inputDelayTimer2 += Time.deltaTime;
                selectSwordP2();
                menuControlsP2();
            }

            menuControlsP1();




            if (p1Ready && p2Ready)
            {
                startMenu.SetActive(true);
            }
        }

        if(swordProfile1P1.activeInHierarchy)
        {
            swordProfile1P1.transform.Rotate(new Vector3(1,0,0));
            swordProfile2P1.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P1.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (swordProfile2P1.activeInHierarchy)
        {
            swordProfile2P1.transform.Rotate(new Vector3(1, 0, 0));
            swordProfile1P1.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P1.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (swordProfile3P1.activeInHierarchy)
        {
            swordProfile3P1.transform.Rotate(new Vector3(1, 0, 0));
            swordProfile1P1.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile2P1.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            swordProfile1P1.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile2P1.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P1.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        if (swordProfile1P2.activeInHierarchy)
        {
            swordProfile1P2.transform.Rotate(new Vector3(1, 0, 0));
            swordProfile2P2.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P2.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (swordProfile2P2.activeInHierarchy)
        {
            swordProfile2P2.transform.Rotate(new Vector3(1, 0, 0));
            swordProfile1P2.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P2.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (swordProfile3P2.activeInHierarchy)
        {
            swordProfile3P2.transform.Rotate(new Vector3(1, 0, 0));
            swordProfile1P2.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile2P2.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else
        {
            swordProfile1P2.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile2P2.transform.rotation = Quaternion.Euler(0, 0, 90);
            swordProfile3P2.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

    }

    //Function to handle player 1 sword selection 
    private void selectSwordP1()
    {
        // if(Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.K)) //LB + A  could do 16 of these if-elseif statements

        if (Input.GetButton("LB_1") || Input.GetKey(KeyCode.Q)) //LB
        {
            leftKeyP1 = true;
            leftKeyNumP1 = 0;

        }
        else if (Input.GetButton("RB_1") || Input.GetKey(KeyCode.E)) //RB
        {
            leftKeyP1 = true;
            leftKeyNumP1 = 1;
        }
        else if ((Input.GetAxis("Triggers_1") > 0.5 && inputDelayTimer >= inputDelay) || Input.GetKey(KeyCode.Alpha1)) //LT
        {
            leftKeyP1 = true;
            leftKeyNumP1 = 2;
            inputDelayTimer = 0.0f;

        }
        else if ((Input.GetAxis("Triggers_1") < -0.5 && inputDelayTimer >= inputDelay) || Input.GetKey(KeyCode.Alpha3)) //RT
        {
            leftKeyP1 = true;
            leftKeyNumP1 = 3;
            inputDelayTimer = 0.0f;

        }
        else
        {
            leftKeyP1 = false;
            leftKeyNumP1 = 0;
        }

        //////////////////////////////////////////////

        if (Input.GetButton("A_1") || Input.GetKey(KeyCode.K)) //A
        {
            rightKeyP1 = true;
            rightKeyNumP1 = 1;

        }
        else if (Input.GetButton("B_1") || Input.GetKey(KeyCode.L)) //B
        {
            rightKeyP1 = true;
            rightKeyNumP1 = 2;

        }
        else if (Input.GetButton("X_1") || Input.GetKey(KeyCode.O)) //X
        {
            rightKeyP1 = true;
            rightKeyNumP1 = 3;

        }
        else if (Input.GetButton("Y_1") || Input.GetKey(KeyCode.P)) //Y
        {
            rightKeyP1 = true;
            rightKeyNumP1 = 4;

        }
        else
        {
            rightKeyP1 = false;
        }


        if (leftKeyP1 && rightKeyP1)
        {
            swordIndexP1 = ((4 * leftKeyNumP1) + rightKeyNumP1) - 1;
            if (swordIndexP1 < 3)
            {
                p1Ready = true;
            }
        }
    }

    private void selectSwordP2()
    {

        if (Input.GetButton("LB_2") || Input.GetKey(KeyCode.Keypad4)) //LB
        {
            leftKeyP2 = true;
            leftKeyNumP2 = 0;

        }
        else if (Input.GetButton("RB_2") || Input.GetKey(KeyCode.Keypad5)) //RB
        {
            leftKeyP2 = true;
            leftKeyNumP2 = 1;

        }
        else if ((Input.GetAxis("Triggers_2") == -1 && inputDelayTimer >= inputDelay) || Input.GetKey(KeyCode.Keypad7)) //LT
        {
            leftKeyP2 = true;
            leftKeyNumP2 = 2;
            inputDelayTimer = 0.0f;
            Debug.Log("LT1");

        }
        else if ((Input.GetAxis("Triggers_2") == -1 && inputDelayTimer >= inputDelay) || Input.GetKey(KeyCode.Keypad8)) //RT
        {
            leftKeyP2 = true;
            leftKeyNumP2 = 3;
            inputDelayTimer = 0.0f;
            Debug.Log("RT1");
        }
        else
        {
            leftKeyP2 = false;
            leftKeyNumP2 = 0;
        }

        /////////////////////////////////////////////////

        if (Input.GetButton("A_2") || Input.GetKey(KeyCode.Keypad1)) //A
        {
            rightKeyP2 = true;
            rightKeyNumP2 = 1;

        }
        else if (Input.GetButton("B_2") || Input.GetKey(KeyCode.Keypad2)) //B
        {
            rightKeyP2 = true;
            rightKeyNumP2 = 2;

        }
        else if (Input.GetButton("X_2") || Input.GetKey(KeyCode.Keypad3)) //X
        {
            rightKeyP2 = true;
            rightKeyNumP2 = 3;

        }
        else if (Input.GetButton("Y_2") || Input.GetKey(KeyCode.Keypad6)) //Y
        {
            rightKeyP2 = true;
            rightKeyNumP2 = 4;

        }
        else
        {
            rightKeyP2 = false;
        }

        if (leftKeyP2 && rightKeyP2)
        {
            swordIndexP2 = ((4 * leftKeyNumP2) + rightKeyNumP2) - 1;
            if (swordIndexP2 < 3)
            {
                p2Ready = true;
            }
        }

    }

    //Function to handle player 1 controller inputs
    void menuControlsP1()
    {
        if ((Input.GetAxis("L_YAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
        {
            if (optionNum > 0)
            {
                switch (optionNum)
                {
                    case 1:
                        tier2Swords[swordIndex].SetActive(false);
                        tier1Swords[swordIndex].SetActive(true);
                        //hard code this part in :(
                        if(swordIndex == 0)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(true);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Length of this Sword is Superior";
                        }
                        else if(swordIndex == 1)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(true);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if(swordIndex == 2)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(true);
                            swordInfoP1.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P1.SetActive(true);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "Unknown Information";
                        }
                        //
                        swordTierIndex = 0;
                        break;
                    case 2:
                        tier3Swords[swordIndex].SetActive(false);
                        tier2Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 1;
                        break;
                    case 3:
                        tier4Swords[swordIndex].SetActive(false);
                        tier3Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 2;
                        break;
                    case 4:
                        roundsCursor.SetActive(false);
                        tier4Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 3;
                        break;
                    case 5:
                        opponentCursor.SetActive(false);
                        roundsCursor.SetActive(true);
                        break;
                }
                if (ladderBool)
                {
                    opponentText.text = swordNames[(swordTierIndex * 4) + swordIndex];
                }
                optionNum--;
            }
            inputDelayTimer = 0.0f;
        }
        else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
        {
            if (optionNum < maxOptionNum)
            {
                switch (optionNum)
                {
                    case 0:
                        tier1Swords[swordIndex].SetActive(false);
                        tier2Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 1;
                        break;
                    case 1:
                        tier2Swords[swordIndex].SetActive(false);
                        tier3Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 2;
                        break;
                    case 2:
                        tier3Swords[swordIndex].SetActive(false);
                        tier4Swords[swordIndex].SetActive(true);
                        //
                        swordProfile0P1.SetActive(true);
                        swordProfile1P1.SetActive(false);
                        swordProfile2P1.SetActive(false);
                        swordProfile3P1.SetActive(false);
                        //
                        swordTierIndex = 3;
                        break;
                    case 3:
                        tier4Swords[swordIndex].SetActive(false);
                        roundsCursor.SetActive(true);
                        break;
                    case 4:
                        roundsCursor.SetActive(false);
                        opponentCursor.SetActive(true);
                        break;
                }
                if (ladderBool)
                {
                    opponentText.text = swordNames[(swordTierIndex * 4) + swordIndex];
                }
                optionNum++;
            }
            inputDelayTimer = 0.0f;
        }
        else if ((Input.GetAxis("L_XAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.A)) //Left key pressed
        {
            if (swordIndex > 0)
            {
                switch (optionNum)
                {
                    case 0: //tier 1 swords
                        tier1Swords[swordIndex].SetActive(false);
                        swordIndex--;
                        tier1Swords[swordIndex].SetActive(true);
                        if (swordIndex == 0)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(true);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Length of this Sword is Superior";
                        }
                        else if (swordIndex == 1)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(true);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if (swordIndex == 2)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(true);
                            swordInfoP1.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P1.SetActive(true);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "Unknown Information";
                        }
                        break;
                    case 1: //tier 2 swords
                        tier2Swords[swordIndex].SetActive(false);
                        swordIndex--;
                        tier2Swords[swordIndex].SetActive(true);
                        break;
                    case 2: //tier 3 swords
                        tier3Swords[swordIndex].SetActive(false);
                        swordIndex--;
                        tier3Swords[swordIndex].SetActive(true);
                        break;
                    case 3: //tier 4 swords
                        tier4Swords[swordIndex].SetActive(false);
                        swordIndex--;
                        tier4Swords[swordIndex].SetActive(true);
                        break;
                }
                if (ladderBool)
                {
                    opponentText.text = swordNames[(swordTierIndex * 4) + swordIndex];
                }
            }
            switch (optionNum)
            {
                case 4: //Rounds
                    if (roundsText.text.CompareTo("3") == 0)
                    {
                        roundsText.text = "9";
                    }
                    else if (roundsText.text.CompareTo("5") == 0)
                    {
                        roundsText.text = "3";
                    }
                    else if (roundsText.text.CompareTo("9") == 0)
                    {
                        roundsText.text = "5";
                    }
                    break;
                case 5: //Opponent
                    if (opponentText.text.CompareTo("2 Player") == 0)
                    {
                        opponentText.text = "AI Hard";
                        p2Ready = true;
                        secondPlayer = false;
                        switch (optionNum2)
                        {
                            case 0:
                                tier1Swords2[swordIndex2].SetActive(false);
                                break;
                            case 1:
                                tier2Swords2[swordIndex2].SetActive(false);
                                break;
                            case 2:
                                tier3Swords2[swordIndex2].SetActive(false);
                                break;
                            case 3:
                                tier4Swords2[swordIndex2].SetActive(false);
                                break;
                        }
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        swordInfoP2.text = "Unknown Information";

                    }
                    else if (opponentText.text.CompareTo("AI Easy") == 0)
                    {
                        opponentText.text = "2 Player";
                        p2Ready = false;
                        secondPlayer = true;
                        switch(optionNum2)
                        {
                            case 0:
                                tier1Swords2[swordIndex2].SetActive(true);
                                if (swordIndex2 == 0)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(true);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "The Length of this Sword is Superior";
                                }
                                else if (swordIndex2 == 1)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(true);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "The Staminia Saved by this Sword is Superior";
                                }
                                else if (swordIndex2 == 2)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(true);
                                    swordInfoP2.text = "The Power of this Sword is Superior";
                                }
                                else
                                {
                                    swordProfile0P2.SetActive(true);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "Unknown Information";
                                }
                                break;
                                
                            case 1:
                                tier2Swords2[swordIndex2].SetActive(true);
                                break;
                            case 2:
                                tier3Swords2[swordIndex2].SetActive(true);
                                break;
                            case 3:
                                tier4Swords2[swordIndex2].SetActive(true);
                                break;
                       }

                    }
                    else if (opponentText.text.CompareTo("AI Medium") == 0)
                    {
                        opponentText.text = "AI Easy";
                    }
                    else if (opponentText.text.CompareTo("AI Hard") == 0)
                    {
                        opponentText.text = "AI Medium";
                    }
                    break;
            }
            inputDelayTimer = 0.0f;
        }
        else if ((Input.GetAxis("L_XAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.D)) //Right key pressed
        {
            if (swordIndex < 3)
            {
                switch (optionNum)
                {
                    case 0: //tier 1 swords
                        tier1Swords[swordIndex].SetActive(false);
                        swordIndex++;
                        tier1Swords[swordIndex].SetActive(true);
                        if (swordIndex == 0)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(true);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Length of this Sword is Superior";
                        }
                        else if (swordIndex == 1)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(true);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if (swordIndex == 2)
                        {
                            swordProfile0P1.SetActive(false);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(true);
                            swordInfoP1.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P1.SetActive(true);
                            swordProfile1P1.SetActive(false);
                            swordProfile2P1.SetActive(false);
                            swordProfile3P1.SetActive(false);
                            swordInfoP1.text = "Unknown Information";
                        }
                        break;
                    case 1: //tier 2 swords
                        tier2Swords[swordIndex].SetActive(false);
                        swordIndex++;
                        tier2Swords[swordIndex].SetActive(true);
                        break;
                    case 2: //tier 3 swords
                        tier3Swords[swordIndex].SetActive(false);
                        swordIndex++;
                        tier3Swords[swordIndex].SetActive(true);
                        break;
                    case 3: //tier 4 swords
                        tier4Swords[swordIndex].SetActive(false);
                        swordIndex++;
                        tier4Swords[swordIndex].SetActive(true);
                        break;
                }
                if (ladderBool)
                {
                    opponentText.text = swordNames[(swordTierIndex * 4) + swordIndex];
                }
            }
            switch (optionNum)
            {
                case 4: //Rounds
                    if (roundsText.text.CompareTo("3") == 0)
                    {
                        roundsText.text = "5";
                    }
                    else if (roundsText.text.CompareTo("5") == 0)
                    {
                        roundsText.text = "9";
                    }
                    else if (roundsText.text.CompareTo("9") == 0)
                    {
                        roundsText.text = "3";
                    }
                    break;
                case 5: //Opponent
                    if (opponentText.text.CompareTo("2 Player") == 0)
                    {
                        opponentText.text = "AI Easy";
                        p2Ready = true;
                        secondPlayer = false;
                        switch (optionNum2)
                        {
                            case 0:
                                tier1Swords2[swordIndex2].SetActive(false);
                                break;
                            case 1:
                                tier2Swords2[swordIndex2].SetActive(false);
                                break;
                            case 2:
                                tier3Swords2[swordIndex2].SetActive(false);
                                break;
                            case 3:
                                tier4Swords2[swordIndex2].SetActive(false);
                                break;
                        }
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        swordInfoP2.text = "Unknown Information";
                    }
                    else if (opponentText.text.CompareTo("AI Easy") == 0)
                    {
                        opponentText.text = "AI Medium";
                    }
                    else if (opponentText.text.CompareTo("AI Medium") == 0)
                    {
                        opponentText.text = "AI Hard";
                    }
                    else if (opponentText.text.CompareTo("AI Hard") == 0)
                    {
                        opponentText.text = "2 Player";
                        p2Ready = false;
                        secondPlayer = true;
                        switch (optionNum2)
                        {
                            case 0:
                                tier1Swords2[swordIndex2].SetActive(true);
                                if (swordIndex2 == 0)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(true);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "The Length of this Sword is Superior";
                                }
                                else if (swordIndex2 == 1)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(true);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "The Staminia Saved by this Sword is Superior";
                                }
                                else if (swordIndex2 == 2)
                                {
                                    swordProfile0P2.SetActive(false);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(true);
                                    swordInfoP2.text = "The Power of this Sword is Superior";
                                }
                                else
                                {
                                    swordProfile0P2.SetActive(true);
                                    swordProfile1P2.SetActive(false);
                                    swordProfile2P2.SetActive(false);
                                    swordProfile3P2.SetActive(false);
                                    swordInfoP2.text = "Unknown Information";
                                }
                                break;
                            case 1:
                                tier2Swords2[swordIndex2].SetActive(true);
                                break;
                            case 2:
                                tier3Swords2[swordIndex2].SetActive(true);
                                break;
                            case 3:
                                tier4Swords2[swordIndex2].SetActive(true);
                                break;
                        }
                    }
                    break;
            }
            inputDelayTimer = 0.0f;
        }
        else if (Input.GetButtonDown("Back_1"))
        {
            if (GameData.gameData.menuType == GameData.Menus.Versus)
            {
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                SceneManager.LoadScene("SinglePlayerMenu");
            }
        }
    }

    //Function to handle player 2 controller inputs
    void menuControlsP2()
    {
        if ((Input.GetAxis("L_YAxis_2") < -0.5f && inputDelayTimer2 >= inputDelay2) || Input.GetKeyDown(KeyCode.UpArrow)) //Up key pressed
        {
            if (optionNum2 > 0)
            {
                switch (optionNum2)
                {
                    case 1:
                        tier2Swords2[swordIndex2].SetActive(false);
                        tier1Swords2[swordIndex2].SetActive(true);
                        //hard code this part in :(
                        if (swordIndex2 == 0)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(true);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Length of this Sword is Superior";
                        }
                        else if (swordIndex2 == 1)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(true);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if (swordIndex2 == 2)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(true);
                            swordInfoP2.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P2.SetActive(true);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "Unknown Information";
                        }
                        //
                        swordTierIndex2 = 0;
                        break;
                    case 2:
                        tier3Swords2[swordIndex2].SetActive(false);
                        tier2Swords2[swordIndex2].SetActive(true);
                        //
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        //
                        swordTierIndex2 = 1;
                        break;
                    case 3:
                        tier4Swords2[swordIndex2].SetActive(false);
                        tier3Swords2[swordIndex2].SetActive(true);
                        //
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        //
                        swordTierIndex2 = 2;
                        break;
                }
                optionNum2--;
            }
            inputDelayTimer2 = 0.0f;
        }
        else if ((Input.GetAxis("L_YAxis_2") > 0.5f && inputDelayTimer2 >= inputDelay2) || Input.GetKeyDown(KeyCode.DownArrow)) //Down key pressed
        {
            if (optionNum2 < maxOptionNum2)
            {
                switch (optionNum2)
                {
                    case 0:
                        tier1Swords2[swordIndex2].SetActive(false);
                        tier2Swords2[swordIndex2].SetActive(true);
                        //
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        //
                        swordTierIndex2 = 1;
                        break;
                    case 1:
                        tier2Swords2[swordIndex2].SetActive(false);
                        tier3Swords2[swordIndex2].SetActive(true);
                        //
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        //
                        swordTierIndex2 = 2;
                        break;
                    case 2:
                        tier3Swords2[swordIndex2].SetActive(false);
                        tier4Swords2[swordIndex2].SetActive(true);
                        //
                        swordProfile0P2.SetActive(true);
                        swordProfile1P2.SetActive(false);
                        swordProfile2P2.SetActive(false);
                        swordProfile3P2.SetActive(false);
                        //
                        swordTierIndex2 = 3;
                        break;
                }
                optionNum2++;
            }
            inputDelayTimer2 = 0.0f;
        }
        else if ((Input.GetAxis("L_XAxis_2") < -0.5f && inputDelayTimer2 >= inputDelay2) || Input.GetKeyDown(KeyCode.LeftArrow)) //Left key pressed
        {
            if (swordIndex2 > 0)
            {
                switch (optionNum2)
                {
                    case 0: //tier 1 swords
                        tier1Swords2[swordIndex2].SetActive(false);
                        swordIndex2--;
                        tier1Swords2[swordIndex2].SetActive(true);
                        if (swordIndex2 == 0)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(true);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Length of this Sword is Superior";
                        }
                        else if (swordIndex2 == 1)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(true);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if (swordIndex2 == 2)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(true);
                            swordInfoP2.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P2.SetActive(true);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "Unknown Information";
                        }
                        break;
                    case 1: //tier 2 swords
                        tier2Swords2[swordIndex2].SetActive(false);
                        swordIndex2--;
                        tier2Swords2[swordIndex2].SetActive(true);
                        break;
                    case 2: //tier 3 swords
                        tier3Swords2[swordIndex2].SetActive(false);
                        swordIndex2--;
                        tier3Swords2[swordIndex2].SetActive(true);
                        break;
                    case 3: //tier 4 swords
                        tier4Swords2[swordIndex2].SetActive(false);
                        swordIndex2--;
                        tier4Swords2[swordIndex2].SetActive(true);
                        break;
                }
            }
            
            inputDelayTimer2 = 0.0f;
        }
        else if ((Input.GetAxis("L_XAxis_2") > 0.5f && inputDelayTimer2 >= inputDelay2) || Input.GetKeyDown(KeyCode.RightArrow)) //Right key pressed
        {
            if (swordIndex2 < 3)
            {
                switch (optionNum2)
                {
                    case 0: //tier 1 swords
                        tier1Swords2[swordIndex2].SetActive(false);
                        swordIndex2++;
                        tier1Swords2[swordIndex2].SetActive(true);
                        if (swordIndex2 == 0)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(true);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Length of this Sword is Superior";
                        }
                        else if (swordIndex2 == 1)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(true);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "The Staminia Saved by this Sword is Superior";
                        }
                        else if (swordIndex2 == 2)
                        {
                            swordProfile0P2.SetActive(false);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(true);
                            swordInfoP2.text = "The Power of this Sword is Superior";
                        }
                        else
                        {
                            swordProfile0P2.SetActive(true);
                            swordProfile1P2.SetActive(false);
                            swordProfile2P2.SetActive(false);
                            swordProfile3P2.SetActive(false);
                            swordInfoP2.text = "Unknown Information";
                        }
                        break;
                    case 1: //tier 2 swords
                        tier2Swords2[swordIndex2].SetActive(false);
                        swordIndex2++;
                        tier2Swords2[swordIndex2].SetActive(true);
                        break;
                    case 2: //tier 3 swords
                        tier3Swords2[swordIndex2].SetActive(false);
                        swordIndex2++;
                        tier3Swords2[swordIndex2].SetActive(true);
                        break;
                    case 3: //tier 4 swords
                        tier4Swords2[swordIndex2].SetActive(false);
                        swordIndex2++;
                        tier4Swords2[swordIndex2].SetActive(true);
                        break;
                }

            }
            inputDelayTimer2 = 0.0f;
        }
        else if (Input.GetButtonDown("Back_2"))
        {
            
        }
    }

    void setGameData()
    {
        GameData.gameData.player1SwordID = swordIndexP1;
        if (GameData.gameData.menuType == GameData.Menus.Ladder)
        {
            GameData.gameData.aiDifficulty = 0;
            GameData.gameData.aiVersus = true;
            GameData.gameData.roundNum = 5;
            LadderController.ladderControl = new LadderController();
        }
        else if (GameData.gameData.menuType == GameData.Menus.Versus)
        {
            GameData.gameData.roundNum = int.Parse(roundsText.text);
            if (opponentText.text.CompareTo("2 Player") == 0)
            {
                GameData.gameData.aiVersus = false;
                GameData.gameData.player2SwordID = swordIndexP2;
            }
            else
            {
                GameData.gameData.aiVersus = true;
                if (opponentText.text.CompareTo("AI Easy") == 0)
                {
                    GameData.gameData.aiDifficulty = 0;
                }
                else if (opponentText.text.CompareTo("AI Medium") == 0)
                {
                    GameData.gameData.aiDifficulty = 1;
                }
                else if (opponentText.text.CompareTo("AI Hard") == 0)
                {
                    GameData.gameData.aiDifficulty = 2;
                }
            }
             
        }
        else
        {
            GameData.gameData.aiDifficulty = 0;
            GameData.gameData.aiVersus = true;
            GameData.gameData.roundNum = int.Parse(roundsText.text);
        }
    }

}
