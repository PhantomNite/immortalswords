﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISettingsMenuManager : MonoBehaviour {

    public GameObject aiSettingsMenu;
    public GameObject aiSetting1Cursor;
    public GameObject aiSetting2Cursor;
    public GameObject confirmCursor;
    public GameObject cancelCursor;


    private GameObject[] menuCursors;
    private int optionNum;

    private float inputDelayTimer;
    private float inputDelay;

    // Use this for initialization
    void Start () {

        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;
        optionNum = 0;

        aiSetting1Cursor.SetActive(true);
        aiSetting2Cursor.SetActive(false);
        confirmCursor.SetActive(false);
        cancelCursor.SetActive(false);

        menuCursors = new GameObject[4];
        menuCursors[0] = aiSetting1Cursor;
        menuCursors[1] = aiSetting2Cursor;
        menuCursors[2] = confirmCursor;
        menuCursors[3] = cancelCursor;



    }
	
	// Update is called once per frame
	void Update () {
		if(aiSettingsMenu.activeInHierarchy)
        {
            inputDelayTimer += Time.deltaTime;
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return))
            {
                switch (optionNum)
                {
                    case 2: //confirm
                        ConfirmOption();
                        CancelOption();
                        break;
                    case 3: //back
                        CancelOption();
                        break;

                }
            }
            else if((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //up key is pressed
            {
                if(optionNum > 0)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum--;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //down key is pressed
            {
                if (optionNum < 3)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum++;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if((Input.GetAxis("L_XAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.A)) //left key pressed
            {
                switch (optionNum)
                {
                    case 0:     //ai setting 1
                        break;

                    case 1:     //ai setting 2
                        break;
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_XAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.D)) //right key pressed
            {
                switch (optionNum)
                {
                    case 0:     //ai setting 1
                        break;

                    case 1:     //ai setting 2
                        break;
                }
                inputDelayTimer = 0.0f;
            }
        }
	}

    void ConfirmOption()
    {
        //save temp AI training 

        //
    }

    void CancelOption()
    {
        optionNum = 0;
        aiSetting1Cursor.SetActive(true);
        aiSetting2Cursor.SetActive(false);
        confirmCursor.SetActive(false);
        cancelCursor.SetActive(false);

        aiSettingsMenu.SetActive(false);

    }
}
