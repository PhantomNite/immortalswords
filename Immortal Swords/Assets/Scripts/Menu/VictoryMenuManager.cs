﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryMenuManager : MonoBehaviour {

    public GameObject victoryMenu;
    public GameObject rematchCursor;
    public GameObject swordSelectCursor;
    public GameObject mainMenuCursor;

    private GameObject[] menuCursors;
    private int optionNum;

    private float inputDelayTimer;
    private float inputDelay;

    // Use this for initialization
    void Start () {
        optionNum = 0;
        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;

        rematchCursor.SetActive(true);
        swordSelectCursor.SetActive(false);
        mainMenuCursor.SetActive(false);

        menuCursors = new GameObject[3];
        menuCursors[0] = rematchCursor;
        menuCursors[1] = swordSelectCursor;
        menuCursors[2] = mainMenuCursor;

    }
	
	// Update is called once per frame
	void Update () {
        if (victoryMenu.activeInHierarchy) {
            inputDelayTimer += Time.deltaTime;
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //If enter is pressed, transition to the select menu
            {
                victoryMenu.SetActive(false);
                if (optionNum == 0) //Rematch
                {
                    SceneManager.LoadScene("Stage");
                }
                else if (optionNum == 1) //Sword Select
                {
                    MenuMusicManager.menuMusic.Play();
                    SceneManager.LoadScene("SwordSelect");
                }
                else //main menu
                {
                    MenuMusicManager.menuMusic.Play();
                    GameData.gameData.menuType = GameData.Menus.Main;
                    SceneManager.LoadScene("MainMenu");
                }
            }
            else if ((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
            {
                if (optionNum > 0)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum--;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
            {
                if (optionNum < 2)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum++;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
        }
    }
}
