﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionMenuManager : MonoBehaviour {

    public GameObject optionMenu;
    public GameObject graphicsMenu;
    public GameObject soundMenu;
    public GameObject gameplayMenu;
    public GameObject saveMenu;
    public Text saveText;

    //Graphics Cursors
    public GameObject windowSizeCursor;
    public GameObject fullscreenCursor;

    //Sound Cursors
    public GameObject masterCursor;
    public GameObject musicCursor;
    public GameObject effectsCursor;

    //Gameplay Cursors

    //Save Menu Cursors
    public GameObject yesCursors;
    public GameObject noCursors;

    //Option Menu Cursors
    public GameObject confirmCursor;
    public GameObject backCursor;
    public GameObject cancelCursor;


    //Graphics Widgets
    public Text windowSizeText;
    public Text fullscreenText;
    private bool fullscreenBool;

    //Sound Widgets
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider effectSlider;


    
    private GameObject currentCursor;
    private GameObject[] graphicsCursors;
    private GameObject[] soundCursors;
    private GameObject[] gameplayCursors;
    private GameObject[] saveMenuCursors;

    private int optionNum;
    private bool saveChanges;

    private float inputDelayTimer;
    private float inputDelay;


    void Awake()
    {
        //for testing!!!!!!!!
        if (GameData.gameData == null)
        {
            GameObject gameObj = new GameObject("GameObject");
            gameObj.AddComponent<GameData>();
            GameData.gameData.menuType = GameData.Menus.Versus;
        }
        if(MenuMusicManager.menuMusic == null)
        {
            GameObject musicObj = new GameObject("MusicObject");
            musicObj.AddComponent<MenuMusicManager>();
        }
    }



    // Use this for initialization
    void Start () {
        //Get values from game data
        windowSizeText.text = GameData.gameData.windowSize;
        fullscreenBool = GameData.gameData.fullscreen;
        if (fullscreenBool)
        {
            fullscreenText.text = "Yes";
        }
        else
        {
            fullscreenText.text = "No";
        }
        masterSlider.value = GameData.gameData.masterVol;
        musicSlider.value = GameData.gameData.musicVol;
        effectSlider.value = GameData.gameData.effectsVol;

        //set default values
        optionNum = 0;
        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;
        saveChanges = false;

        graphicsMenu.SetActive(true);
        soundMenu.SetActive(false);
        gameplayMenu.SetActive(false);
        saveMenu.SetActive(false);


        //set up cursor group arrays
        graphicsCursors = new GameObject[5];
        graphicsCursors[0] = windowSizeCursor;
        graphicsCursors[1] = fullscreenCursor;
        graphicsCursors[2] = confirmCursor;
        graphicsCursors[3] = backCursor;
        graphicsCursors[4] = cancelCursor;

        soundCursors = new GameObject[6];
        soundCursors[0] = masterCursor;
        soundCursors[1] = musicCursor;
        soundCursors[2] = effectsCursor;
        soundCursors[3] = confirmCursor;
        soundCursors[4] = backCursor;
        soundCursors[5] = cancelCursor;

        gameplayCursors = new GameObject[3];
        gameplayCursors[0] = confirmCursor;
        gameplayCursors[1] = backCursor;
        gameplayCursors[2] = cancelCursor;

        saveMenuCursors = new GameObject[2];
        saveMenuCursors[0] = yesCursors;
        saveMenuCursors[1] = noCursors;

        //set default cursors statuses
        currentCursor = windowSizeCursor;
        fullscreenCursor.SetActive(false);
        masterCursor.SetActive(false);
        musicCursor.SetActive(false);
        effectsCursor.SetActive(false);
        confirmCursor.SetActive(false);
        backCursor.SetActive(false);
        cancelCursor.SetActive(false);
        noCursors.SetActive(false);


    }
	
	// Update is called once per frame
	void Update () {
        if (optionMenu.activeInHierarchy)
        {
            inputDelayTimer += Time.deltaTime;
            if (graphicsMenu.activeInHierarchy)
            {
                //to switch between the different option menus
                if (Input.GetButtonDown("RB_1") || Input.GetKeyDown(KeyCode.E)) //temp for RB to switch to 'Sound Menu'
                {
                    graphicsMenu.SetActive(false);
                    soundMenu.SetActive(true);
                    graphicsCursors[optionNum].SetActive(false);
                    soundCursors[0].SetActive(true);
                    currentCursor = soundCursors[0];
                    optionNum = 0;
                }
                //to cycle between graphic menu options
                else if ( (Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
                {
                    if (optionNum > 0)
                    {
                        graphicsCursors[optionNum].SetActive(false);
                        optionNum--;
                        graphicsCursors[optionNum].SetActive(true);
                        currentCursor = graphicsCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
                {
                    if (optionNum < 4)
                    {
                        graphicsCursors[optionNum].SetActive(false);
                        optionNum++;
                        graphicsCursors[optionNum].SetActive(true);
                        currentCursor = graphicsCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_XAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.A)) //left key press
                {
                    switch (optionNum)
                    {
                        case 0: //Window Size
                            //1920 x 1080
                            //1280 x  720
                            // 854 x  480
                            if (windowSizeText.text.CompareTo("1920 x 1080") == 0)
                            {
                                windowSizeText.text = "1280 x  720";
                            }
                            else if (windowSizeText.text.CompareTo("1280 x  720") == 0)
                            {
                                windowSizeText.text = "854 x 480";
                            }
                            else if (windowSizeText.text.CompareTo("854 x 480") == 0)
                            {
                                windowSizeText.text = "1920 x 1080";
                            }
                            saveChanges = true;
                            break;
                        case 1: //Fullscreen
                            fullscreenBool = !fullscreenBool;
                            if (fullscreenBool)
                            {
                                fullscreenText.text = "Yes";
                            }
                            else
                            {
                                fullscreenText.text = "No";
                            }
                            saveChanges = true;
                            break;
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_XAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.D)) //right key press
                {
                    switch (optionNum)
                    {
                        case 0: //Window Size

                            //1920 x 1080
                            //1280 x  720
                            // 854 x  480
                            if (windowSizeText.text.CompareTo("1920 x 1080") == 0)
                            {
                                windowSizeText.text = "854 x 480";
                            }
                            else if (windowSizeText.text.CompareTo("1280 x  720") == 0)
                            {
                                windowSizeText.text = "1920 x 1080";
                            }
                            else if (windowSizeText.text.CompareTo("854 x 480") == 0)
                            {
                                windowSizeText.text = "1280 x  720";
                            }
                            saveChanges = true;
                            break;
                        case 1: //Fullscreen
                            fullscreenBool = !fullscreenBool;
                            if (fullscreenBool)
                            {
                                fullscreenText.text = "Yes";
                            }
                            else
                            {
                                fullscreenText.text = "No";
                            }
                            saveChanges = true;
                            break;

                    }
                    inputDelayTimer = 0.0f;
                }

                //when selecting the option
                else if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //select
                {
                    switch (optionNum)
                    {
                        case 2: //confirm
                            ConfirmOption();
                            break;
                        case 3: //back
                            graphicsMenu.SetActive(false);
                            BackOption();
                            break;
                        case 4: //cancel
                            CancelOption();
                            break;
                    }
                }

            }
            else if (soundMenu.activeInHierarchy)
            {
                if (Input.GetButtonDown("LB_1") || Input.GetKeyDown(KeyCode.Q)) //temp for LB to switch to 'Graphics Menu'
                {
                    soundMenu.SetActive(false);
                    graphicsMenu.SetActive(true);
                    soundCursors[optionNum].SetActive(false);
                    graphicsCursors[0].SetActive(true);
                    currentCursor = graphicsCursors[0];
                    optionNum = 0;
                }
                else if (Input.GetButtonDown("RB_1") || Input.GetKeyDown(KeyCode.E)) //temp for RB to switch to 'Gameplay Menu'
                {
                    soundMenu.SetActive(false);
                    gameplayMenu.SetActive(true);
                    soundCursors[optionNum].SetActive(false);
                    gameplayCursors[0].SetActive(true);
                    currentCursor = gameplayCursors[0];
                    optionNum = 0;
                }
                //to cycle between graphic menu options
                else if ((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
                {
                    if (optionNum > 0)
                    {
                        soundCursors[optionNum].SetActive(false);
                        optionNum--;
                        soundCursors[optionNum].SetActive(true);
                        currentCursor = soundCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
                {
                    if (optionNum < 5)
                    {
                        soundCursors[optionNum].SetActive(false);
                        optionNum++;
                        soundCursors[optionNum].SetActive(true);
                        currentCursor = soundCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }

                else if ((Input.GetAxis("L_XAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.A)) //left key pressed
                {
                    switch (optionNum)
                    {
                        case 0: //Master Volume
                            masterSlider.value -= 5;
                            MenuMusicManager.menuMusic.UpdateVolume((musicSlider.value / 100.0f) * (masterSlider.value / 100.0f));
                            saveChanges = true;
                            break;
                        case 1: //Music Volume
                            musicSlider.value -= 5;
                            MenuMusicManager.menuMusic.UpdateVolume((musicSlider.value / 100.0f) * (masterSlider.value / 100.0f));
                            saveChanges = true;
                            break;
                        case 2: //Effects Volume
                            effectSlider.value -= 5;
                            saveChanges = true;
                            break;
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_XAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.D)) //right key pressed
                {
                    switch (optionNum)
                    {
                        case 0: //Master Volume
                            masterSlider.value += 5;
                            MenuMusicManager.menuMusic.UpdateVolume((musicSlider.value / 100.0f) * (masterSlider.value / 100.0f));
                            saveChanges = true;
                            break;
                        case 1: //Music Volume
                            musicSlider.value += 5;
                            MenuMusicManager.menuMusic.UpdateVolume((musicSlider.value / 100.0f) * (masterSlider.value / 100.0f));
                            saveChanges = true;
                            break;
                        case 2: //Effects Volume
                            effectSlider.value += 5;
                            saveChanges = true;
                            break;
                    }
                    inputDelayTimer = 0.0f;
                }

                //when selecting the option
                else if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //select
                {
                    switch (optionNum)
                    {
                        case 3: //confirm
                            ConfirmOption();
                            break;
                        case 4: //back
                            soundMenu.SetActive(false);
                            BackOption();
                            break;
                        case 5: //cancel
                            CancelOption();
                            break;
                    }
                }

            }
            else if (gameplayMenu.activeInHierarchy)
            {
                if (Input.GetButtonDown("LB_1") || Input.GetKeyDown(KeyCode.Q)) //temp for LB to switch to 'Sound Menu'
                {
                    gameplayMenu.SetActive(false);
                    soundMenu.SetActive(true);
                    gameplayCursors[optionNum].SetActive(false);
                    soundCursors[0].SetActive(true);
                    currentCursor = soundCursors[0];
                    optionNum = 0;
                }
                //to cycle between graphic menu options
                else if ((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
                {
                    if (optionNum > 0)
                    {
                        gameplayCursors[optionNum].SetActive(false);
                        optionNum--;
                        gameplayCursors[optionNum].SetActive(true);
                        currentCursor = gameplayCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
                {
                    if (optionNum < 2)
                    {
                        gameplayCursors[optionNum].SetActive(false);
                        optionNum++;
                        gameplayCursors[optionNum].SetActive(true);
                        currentCursor = gameplayCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                //when selecting the option
                else if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //select
                {
                    switch (optionNum)
                    {
                        case 0: //confirm
                            ConfirmOption();
                            break;
                        case 1: //back
                            gameplayMenu.SetActive(false);
                            BackOption();
                            break;
                        case 2: //cancel
                            CancelOption();
                            break;
                    }
                }



            }
            else if (saveMenu.activeInHierarchy)
            {
                 //to cycle between graphic menu options
                if ((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
                {
                    if (optionNum > 0)
                    {
                        saveMenuCursors[optionNum].SetActive(false);
                        optionNum--;
                        saveMenuCursors[optionNum].SetActive(true);
                        currentCursor = saveMenuCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
                {
                    if (optionNum < 1)
                    {
                        saveMenuCursors[optionNum].SetActive(false);
                        optionNum++;
                        saveMenuCursors[optionNum].SetActive(true);
                        currentCursor = saveMenuCursors[optionNum];
                    }
                    inputDelayTimer = 0.0f;
                }
                //when selecting the option
                else if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //select
                {
                    switch (optionNum)
                    {
                        case 0: //yes
                            saveMenu.SetActive(false);
                            graphicsMenu.SetActive(true);
                            optionNum = 4;
                            graphicsCursors[4].SetActive(true);
                            currentCursor = graphicsCursors[4];
                            ConfirmOption();
                            break;
                        case 1: //no
                            saveMenu.SetActive(false);
                            CancelOption();
                            break;
                    }
                }
            }
            
        }
    }


    private void ConfirmOption()
    {
        //use data save here
        GameData.gameData.windowSize = windowSizeText.text;
        GameData.gameData.fullscreen = fullscreenBool;
        GameData.gameData.masterVol = masterSlider.value;
        GameData.gameData.musicVol = musicSlider.value;
        GameData.gameData.effectsVol = effectSlider.value;
        GameData.gameData.SaveOptionData();

        //1: window size & //2: fullscreen
        if (windowSizeText.text.CompareTo("1920 x 1080") == 0)
        {
            Screen.SetResolution(1920, 1080, fullscreenBool);
        }
        else if (windowSizeText.text.CompareTo("1280 x  720") == 0)
        {
            Screen.SetResolution(1280, 720, fullscreenBool);
        }
        else if (windowSizeText.text.CompareTo("854 x 480") == 0)
        {
            Screen.SetResolution(854, 480, fullscreenBool);
        }

        //4: master volume
        //masterSlider.value;

        //5: music volume
        //musicSlider.value;

        //6: effects volume
        //effectSlider.value;
        StartCoroutine(Fade());
    }

    private void BackOption()
    {
        if (saveChanges)
        {
            //ask to save changes before going back
            saveMenu.SetActive(true);
            currentCursor.SetActive(false);
            yesCursors.SetActive(true);
            optionNum = 0;

            saveChanges = false;
        }
        else
        {
            CancelOption();
        }
    }

    private void CancelOption()
    {

        //Revert settings
        MenuMusicManager.menuMusic.UpdateVolume((GameData.gameData.musicVol / 100.0f) * (GameData.gameData.masterVol / 100.0f));

        //set default values
        optionNum = 0;
        saveChanges = false;
        graphicsMenu.SetActive(true);
        soundMenu.SetActive(false);
        gameplayMenu.SetActive(false);
        saveMenu.SetActive(false);
        //set default cursors statuses
        currentCursor = windowSizeCursor;
        windowSizeCursor.SetActive(true);
        fullscreenCursor.SetActive(false);
        masterCursor.SetActive(false);
        musicCursor.SetActive(false);
        effectsCursor.SetActive(false);
        confirmCursor.SetActive(false);
        backCursor.SetActive(false);
        cancelCursor.SetActive(false);
        yesCursors.SetActive(false);
        noCursors.SetActive(false);



        optionMenu.SetActive(false);
        
    }


    IEnumerator Fade()
    {
        for (float f = 1.0f; f >= 0; f -= 0.2f)
        {
            Color color = saveText.color;
            color.a = f;
            saveText.color = color;
            yield return new WaitForSeconds(0.1f);
        }
    }

}
