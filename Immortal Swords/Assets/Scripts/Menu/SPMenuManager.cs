﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SPMenuManager : MonoBehaviour {
    public GameObject ladderCursor;
    public GameObject trainingCursor;
    public GameObject tutorialCursor;
    public GameObject mainMenuCursor;

    private GameObject[] menuCursors;
    private int optionNum;

    private float inputDelayTimer;
    private float inputDelay;

    // Use this for initialization
    void Start () {
        optionNum = 0;
        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;

        menuCursors = new GameObject[4];
        menuCursors[0] = ladderCursor;
        menuCursors[1] = trainingCursor;
        menuCursors[2] = tutorialCursor;
        menuCursors[3] = mainMenuCursor;

        menuCursors[0].SetActive(true);
        menuCursors[1].SetActive(false);
        menuCursors[2].SetActive(false);
        menuCursors[3].SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        inputDelayTimer += Time.deltaTime;
        if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //If enter is pressed, transition to the select menu
        {
            if (optionNum == 0) //Ladder
            {
                //player data set stage to ladder
                GameData.gameData.menuType = GameData.Menus.Ladder;
                SceneManager.LoadScene("SwordSelect");
            }
            else if (optionNum == 1) //Training
            {
                //player data set stage training
                //GameData.gameData.menuType = GameData.Menus.Training;
                //SceneManager.LoadScene("SwordSelect");
            }
            else if (optionNum == 2) //Tutorial
            {
                //player data set stage tutorial
                //GameData.gameData.menuType = GameData.Menus.Tutorial;
                //SceneManager.LoadScene("SwordSelect");
            }
            else //main menu
            {
                GameData.gameData.menuType = GameData.Menus.Main;
                SceneManager.LoadScene("MainMenu");
            }


        }
        else if ((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
        {
            if (optionNum > 0)
            {
                menuCursors[optionNum].SetActive(false);
                optionNum--;
                menuCursors[optionNum].SetActive(true);
            }
            inputDelayTimer = 0.0f;
        }
        else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
        {
            if (optionNum < 3)
            {
                menuCursors[optionNum].SetActive(false);
                optionNum++;
                menuCursors[optionNum].SetActive(true);
            }
            inputDelayTimer = 0.0f;
        }
    }
}

