﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    public GameObject optionMenu;
    public GameObject singleplayerCursor;
    public GameObject versusCursor;
    public GameObject optionCursor;
    public GameObject quitCursor;

    private GameObject[] menuCursors;

    private int optionNum;

    private float inputDelayTimer;
    private float inputDelay;

	// Use this for initialization
	void Start () {
        //Load and set up options from save file
        setUpOptions();

        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;
        optionNum = 0;

        //create array to hold cursor GameObjects
        menuCursors = new GameObject[4];
        menuCursors[0] = singleplayerCursor;
        menuCursors[1] = versusCursor;
        menuCursors[2] = optionCursor;
        menuCursors[3] = quitCursor;

        //Hide the objects not needed in the start
        optionMenu.SetActive(false);
        menuCursors[0].SetActive(true);
        menuCursors[1].SetActive(false);
        menuCursors[2].SetActive(false);
        menuCursors[3].SetActive(false);

    }
	

// Update is called once per frame
void Update () {
        if (!(optionMenu.activeInHierarchy)) {
            inputDelayTimer += Time.deltaTime;
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //If enter is pressed, transition to the select menu
            {
                if (optionNum == 0) //Single Player
                {
                    GameData.gameData.menuType = GameData.Menus.SinglePlayer;
                    SceneManager.LoadScene("SinglePlayerMenu");   
                }
                else if (optionNum == 1) //Versus
                {
                    GameData.gameData.menuType = GameData.Menus.Versus;
                    SceneManager.LoadScene("SwordSelect");
                }
                else if (optionNum == 2) //Options
                {
                    optionMenu.SetActive(true);
                }
                else //quit
                {
                    Application.Quit();
                }

            }
            else if ((Input.GetAxis("L_YAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //Up key pressed
            {
                if (optionNum > 0)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum--;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
            {
                if (optionNum < 3)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum++;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
        }
    }

    void setUpOptions()
    {
        if(!GameData.gameData.loadedOptions)
        {
            GameData.gameData.LoadOptionData();
        }

        //1: window size & //2: fullscreen
        if (GameData.gameData.windowSize.CompareTo("1920 x 1080") == 0)
        {
            Screen.SetResolution(1920, 1080, GameData.gameData.fullscreen);
        }
        else if (GameData.gameData.windowSize.CompareTo("1280 x  720") == 0)
        {
            Screen.SetResolution(1280, 720, GameData.gameData.fullscreen);
        }
        else if (GameData.gameData.windowSize.CompareTo("854 x 480") == 0)
        {
            Screen.SetResolution(854, 480, GameData.gameData.fullscreen);
        }

        //3: brightness
        //brightnessSlider.value;

        //4: master volume
        //masterSlider.value;

        //5: music volume
        //musicSlider.value;

        //6: effects volume
        //effectSlider.value;


    }

}
