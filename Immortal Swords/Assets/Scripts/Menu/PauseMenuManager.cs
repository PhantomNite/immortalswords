﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour {



    //Menu Cursors
    public GameObject resumeCursor;
    public GameObject optionsCursor;
    public GameObject aISettingsCursor;
    public GameObject mainMenuCursor;
    public GameObject aiSettingsText;

    public GameObject pauseMenu;
    public GameObject optionsMenu;
    public GameObject aiSettingsMenu;

    private GameObject[] menuCursors;

    private bool trainingMode;
    private int optionNum;

    private float inputDelayTimer;
    private float inputDelay;

    //For TESTING
    void Awake()
    {
        if (GameData.gameData == null)
        {
            GameObject gameObj = new GameObject("GameObject");
            gameObj.AddComponent<GameData>();
            GameData.gameData.menuType = GameData.Menus.Training;
        }
        
    }


    void Start ()
    {
        trainingMode = false;
        optionNum = 0;
        inputDelayTimer = 0.0f;
        inputDelay = 0.15f;

        if (GameData.gameData.menuType == GameData.Menus.Training)
        {
            trainingMode = true;
        }

        resumeCursor.SetActive(true);
        optionsCursor.SetActive(false);
        aISettingsCursor.SetActive(false);
        mainMenuCursor.SetActive(false);

        optionsMenu.SetActive(false);
        aiSettingsMenu.SetActive(false);

        if (trainingMode)
        {
            menuCursors = new GameObject[4];
            menuCursors[0] = resumeCursor;
            menuCursors[1] = optionsCursor;
            menuCursors[2] = aISettingsCursor;
            menuCursors[3] = mainMenuCursor;
            aiSettingsText.SetActive(true);
        }
        else
        {
            menuCursors = new GameObject[3];
            menuCursors[0] = resumeCursor;
            menuCursors[1] = optionsCursor;
            menuCursors[2] = mainMenuCursor;
            aiSettingsText.SetActive(false);
        }

    }
	
	// Update is called once per frame
	void Update ()
    {   
        if (pauseMenu.activeInHierarchy && !(optionsMenu.activeInHierarchy) && !(aiSettingsMenu.activeInHierarchy))
        {
            inputDelayTimer += Time.deltaTime;
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //enter key pressed
            {
                if (optionNum == 0) //Resume
                {
                    pauseMenu.SetActive(false);
                }
                else if (optionNum == 1) //Options
                {
                    optionsMenu.SetActive(true);
                }
                else if (optionNum == 2) //AI / Main Menu
                {
                    if (trainingMode)
                    {
                        aiSettingsMenu.SetActive(true);
                    }
                    else
                    {
                        MenuMusicManager.menuMusic.Play();
                        SceneManager.LoadScene("MainMenu");
                    }
                }
                else //quit
                {
                    MenuMusicManager.menuMusic.Play();
                    SceneManager.LoadScene("MainMenu");
                }
            }
            else if((Input.GetAxis("L_YAxis_1") < -0.5f&& inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //up key
            {
                if (optionNum > 0)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum--;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
            {
                if (optionNum < menuCursors.Length - 1)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum++;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if (Input.GetButtonDown("Start_1") || Input.GetKeyDown(KeyCode.P))
            {
                pauseMenu.SetActive(false);
            }
        }

        //for testing until pause option in manager
        else
        {
            if(Input.GetButtonDown("Start_1") || Input.GetKeyDown(KeyCode.P))
            {
                pauseMenu.SetActive(true);
            }
        }
	}
}
