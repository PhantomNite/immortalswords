﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object
    public GameObject player2;
    public GameObject gameManager;
    private Vector3 center;


    private Vector3 offset;         //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start () 
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        center = new Vector3(0, player.transform.position.y, player.transform.position.z);
        offset = transform.position - center;
        
    }
    
    // LateUpdate is called after Update each frame
    void LateUpdate () 
    {
        //the distance of the midpoint
        float dist = (player.transform.position.x + player2.transform.position.x);
        if (dist != 0)
            dist = dist / 2.0f;
        //Debug.Log("Midpoint to other: " + dist);

        //center
        center.x = dist;
        
        //Debug.Log(offset);
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        transform.position = center + offset;
    }
}