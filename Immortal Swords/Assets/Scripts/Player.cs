﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    public SpriteRenderer renderer;
    public Animator anime;
    public GameObject opponent;
    Player method;
    public GameObject sword;
    public Image stamina;
    public float stagDelay;
    public float stagDelayLimit;
    public GameObject gameManager;
    GameManager manager;
    public PolygonCollider2D[] hitboxes;
    public int stance;
    //Weapon mods
    public float lightCost_Mod;
    public float heavyCost_Mod;
    public float blockCost_Mod;

    public float lightAttack_Mod;
    public float heavyAttack_Mod;

    public bool facingLeft;
    public bool staggered;
    public bool blocking;
    public bool lightAttacking;
    public bool heavyAttacking;
    float speed = 10.0f;

    //stamina cost
    float lightPenalty;
    float heavyPenalty;
    float blockPenalty;

    //animation delays
    float light_attack_timeLimit;
    float light_attack_timer;

    //Color
    Color norm;

    //player identification
    public int id;

    //hazard length for environment death
    float hazard;

    //evade timer
    float evadeDelay;
    float evadeDelayTimer;


    // Use this for initialization
    void Start () {
        anime = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        lightPenalty = -0.10f;
        heavyPenalty = -0.20f;
        blockPenalty = -0.05f;
        blocking = true;
        staggered = false;
        lightAttacking = false;
        heavyAttacking = false;
        stagDelay = 1.0f;
        stagDelayLimit = 1.0f;
        light_attack_timeLimit = 0.4f;
        light_attack_timer = light_attack_timeLimit;
        method = (Player)opponent.GetComponent(typeof(Player));
        norm = renderer.color;
        gameManager = GameObject.FindGameObjectWithTag("GAMEMANAGER").gameObject;
        manager = (GameManager)gameManager.GetComponent(typeof(GameManager));
        hazard = 33.0f;
        evadeDelayTimer = 1.0f;
        evadeDelay = evadeDelayTimer;
        stance = 0;
        
    }
	
	// Update is called once per frame
	void Update () {

        light_attack_timer -= Time.deltaTime;
        evadeDelay += Time.deltaTime;

        
        if (blocking)
        {
            //renderer.color = new Color(255.0f, 255.0f, 0.0f);
            if (light_attack_timer < 0)
            {
                lightAttacking = false;
                heavyAttacking = false;
                anime.SetBool("attacking", lightAttacking);
            }
        }
        if(staggered)
        {
            //renderer.color = new Color(0.0f, 255.0f, 0.0f);
            stagDelay -= Time.deltaTime;
            if(stagDelay <= 0)
            {
                stagDelay = stagDelayLimit;
                staggered = false;
                blocking = true;
                renderer.color = norm;
            }
        }

        if (transform.position.x >= hazard || transform.position.x <= -hazard)
            manager.addWin(method.id);

        //checkDirection();
        //evading = false;

        anime.SetBool("staggering", staggered);

        if (GetComponent<Rigidbody2D>().velocity.x == 0)
        {
            anime.SetBool("walking", false);
        }
        else
        {
            anime.SetBool("walking", true);
        }


    }

    public void left()
    {
        anime.SetBool("blocking", false);
        var vel = GetComponent<Rigidbody2D>().velocity;
        vel.x = -speed;

        float dist = Vector3.Distance(opponent.transform.position, transform.position);
        if ((dist > 15 && opponent.transform.position.x > transform.position.x) ||
            (anime.GetCurrentAnimatorStateInfo(0).IsName("Medium Attack") ||
            anime.GetCurrentAnimatorStateInfo(0).IsName("Low Attack") ||
            anime.GetCurrentAnimatorStateInfo(0).IsName("High Attack") ||
            anime.GetCurrentAnimatorStateInfo(0).IsName("Staggered")) ||
            (dist < 5 && (opponent.transform.position.x < transform.position.x)))
            vel.x = 0;
        
        GetComponent<Rigidbody2D>().velocity = vel;
    }

    public void right()
    {
        anime.SetBool("blocking", false);
        var vel = GetComponent<Rigidbody2D>().velocity;
        vel.x = speed;

        float dist = Vector3.Distance(opponent.transform.position, transform.position);
        if ((dist > 15 && opponent.transform.position.x < transform.position.x) ||
             (anime.GetCurrentAnimatorStateInfo(0).IsName("Medium Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Low Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("High Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Staggered")) ||
             (dist < 5 && (opponent.transform.position.x > transform.position.x)))
            vel.x = 0;
        GetComponent<Rigidbody2D>().velocity = vel;
    }

    public void stanceUp()
    {
        if (stance == 0)
            stance = 1;
        else if (stance == 1)
            stance = -1;
        else
            stance = 0;
        anime.SetInteger("state", stance);
    }

    public void stanceDown()
    {
        if (stance == 0)
            stance = -1;
        else if (stance == 1)
            stance = 0;
        else
            stance = 1;
        anime.SetInteger("state", stance);
    }

    public void updateBar(float num)
    {
        //if not staggerd or evading then add value
        if (staggered == false && evadeDelay >= evadeDelayTimer)
        {
            hitboxes[1].isTrigger = false;
            stamina.fillAmount = stamina.fillAmount + num;
        }
        //if stamina empty then set to staggered
        if (stamina.fillAmount == 0.0f)
        {
            //knockback();
            staggered = true;
            blocking = false;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //********************light attack*****************************************
        if(col == method.hitboxes[1] && lightAttacking)// hits sword
        {
            if(method.lightAttacking)
            {
                //counter system
                if (isStanceSuperior(method.stance))//superior stance
                {
                    method.updateBar(lightAttack_Mod + lightPenalty * 2.0f);
                    Debug.Log("Player "+ id + " Superior");
                }
                else if (stance == method.stance)//equal stance
                {
                    //no loss in stamina
                    //updateBar(-lightPenalty);
                    //method.updateBar(-lightPenalty);
                    Debug.Log("Player " + id + " Even");
                }
                else//inferior stance
                {
                    updateBar(lightAttack_Mod + lightPenalty * 2.0f);
                    Debug.Log("Player " + id + " Inferior");
                }
                //Debug.Log("Countered");
            }
            else if (method.isBlocking())
            {
                method.anime.SetBool("blocking", true);
                method.updateBar(lightAttack_Mod + blockPenalty);
            }
        }
        //********************heavy attack*****************************************
        else if (col == method.hitboxes[1] && heavyAttacking)// hits sword
        {
            if (method.heavyAttacking)
            {
                //counter system
                if (isStanceSuperior(method.stance))//superior stance
                {
                    method.updateBar(heavyAttack_Mod + heavyPenalty * 2.0f);
                    Debug.Log("Player " + id + " Superior");
                }
                else if (stance == method.stance)//equal stance
                {
                    //no loss in stamina
                    //updateBar(-heavyPenalty);
                    //method.updateBar(-heavyPenalty);
                    Debug.Log("Player " + id + " Even");
                }
                else//inferior stance
                {
                    updateBar(heavyAttack_Mod + heavyPenalty * 2.0f);
                    Debug.Log("Player " + id + " Inferior");
                }
                //Debug.Log("Countered");
            }
            else if (method.isBlocking())
            {
                method.anime.SetBool("blocking", true);
                method.updateBar(heavyAttack_Mod + heavyPenalty * 2.0f);
            }
            //else if (method.staggered)
            //{
            //    manager.addWin(id);
            //}
            //Debug.Log("Player " + id + " hit sword");
        }
        if (col == method.hitboxes[1] && (lightAttacking || heavyAttacking) )// hits body
        {
            if (method.staggered && method.stagDelay < 0.5f)
            {
                manager.addWin(id);
            }
            Debug.Log("Player " + id + " hit body");
        }
            
    }

    public void LightAttack()
    {
        blocking = false;

        //animation
        if (!staggered && evadeDelay >= evadeDelayTimer)
        {
            lightAttacking = true;
            light_attack_timer = light_attack_timeLimit;
            anime.SetBool("blocking", false);
            anime.SetBool("attacking", lightAttacking);
            if(stamina.fillAmount + lightPenalty + lightCost_Mod> 0)
                updateBar(lightPenalty + lightCost_Mod);
        }
        
    }
    
    public void HeavyAttack()
    {
        blocking = false;

        //animation
        if(!staggered && evadeDelay >= evadeDelayTimer)
        {
            heavyAttacking = true;
            light_attack_timer = light_attack_timeLimit;
            anime.SetBool("blocking", false);
            anime.SetBool("attacking", heavyAttacking);
            if (stamina.fillAmount + heavyPenalty + heavyAttack_Mod> 0)
                updateBar(heavyPenalty + heavyAttack_Mod);
        }
        
        //renderer.color = new Color(255.0f, 0.0f, 0.0f);
    }

    public void block()
    {
        blocking = true;
    }
    //*****************************evasion methods****************************
    
    //going to replace teleport with actual movement
    public void flak()
    {
        float y = transform.position.y;
        float z = transform.position.z;
        float x = opponent.transform.position.x + 5.0f;

        if (opponent.transform.position.x < transform.position.x)
        {
            x = opponent.transform.position.x - 5.0f;
        }
        
        transform.position = new Vector3(x, y, z);
    }

    //public void flak_from_left()
    //{
    //    float y = transform.position.y;
    //    float z = transform.position.z;
    //    float x = opponent.transform.position.x + 5.0f;
    //    transform.position = new Vector3(x, y, z);
    //}

    //public void flak_from_right()
    //{
    //    float y = transform.position.y;
    //    float z = transform.position.z;
    //    float x = opponent.transform.position.x - 5.0f;
    //    transform.position = new Vector3(x, y, z);
    //}


    public void evadeLeft()
    {

        if (anime.GetCurrentAnimatorStateInfo(0).IsName("Medium Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Low Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("High Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Staggered")) return;
            hitboxes[1].isTrigger = true;

        var vel = GetComponent<Rigidbody2D>().velocity;
        vel.x = -speed * 5.0f;

        float dist = Vector3.Distance(opponent.transform.position, transform.position);
        if ((dist > 15) && (opponent.transform.position.x > transform.position.x))
            vel.x = 0;

        if (dist <= 5)//&& (transform.position.x > 15)
            vel *= 3.0f;

        GetComponent<Rigidbody2D>().velocity = vel;

        evadeDelay = 0;
    }

    public void evadeRight()
    {

        if (anime.GetCurrentAnimatorStateInfo(0).IsName("Medium Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Low Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("High Attack") ||
             anime.GetCurrentAnimatorStateInfo(0).IsName("Staggered")) return;
        hitboxes[1].isTrigger = true;

        var vel = GetComponent<Rigidbody2D>().velocity;
        vel.x = speed * 5.0f;

        float dist = Vector3.Distance(opponent.transform.position, transform.position);
        if ((dist > 15) && (opponent.transform.position.x > transform.position.x))
            vel.x = 0;
        if (dist <= 5) //&& (transform.position.x > 15)
            vel *= 3.0f;


        GetComponent<Rigidbody2D>().velocity = vel;

        evadeDelay = 0;
    }
    //*********************************************************************************************************
    public bool isBlocking()
    {
        return blocking;
    }

    public bool isStanceSuperior(int s)
    {
        if (s == 1 && stance == 0)
            return true;
        else if (s == 0 && stance == -1)
            return true;
        else if (s == -1 && stance == 1)
            return true;
        else
            return false;
    }

    public void isPlayerFacingLeft(bool dir)
    {
        facingLeft = dir;
    }

    public void checkDirection()
    {
        //is the player facing his opponent
        if ((opponent.transform.position.x < transform.position.x) && !facingLeft)
        {
            facingLeft = true;
            transform.Rotate(new Vector3(0, 180, 0));//localScale = new Vector3(-1, 1, 1);
        }
        else if ((opponent.transform.position.x > transform.position.x) && facingLeft)
        {
            facingLeft = false;
            transform.Rotate(new Vector3(0, 180, 0));//.localScale = new Vector3(1, 1, 1);
        }




    }

    public void knockback()
    {
        Debug.Log("knock back");
        if(facingLeft)
        {
            var vel = GetComponent<Rigidbody2D>().velocity;
            vel.x = 5.0f * speed;

            float dist = Vector3.Distance(opponent.transform.position, transform.position);
            if ((dist > 15 && opponent.transform.position.x < transform.position.x) ||
                 (anime.GetCurrentAnimatorStateInfo(0).IsName("Medium Attack") ||
                 anime.GetCurrentAnimatorStateInfo(0).IsName("Low Attack") ||
                 anime.GetCurrentAnimatorStateInfo(0).IsName("High Attack") ||
                 anime.GetCurrentAnimatorStateInfo(0).IsName("Staggered")) ||
                 (dist < 5 && (opponent.transform.position.x > transform.position.x)))
                vel.x = 0;
            GetComponent<Rigidbody2D>().velocity = vel;
        }
        else
        {
            left();
        }
        staggered = true;
    }
}
