﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicManager : MonoBehaviour {

    public static MenuMusicManager menuMusic;
    public AudioClip menuMusicClip;


    private AudioSource audioSource;
    private float musicPitch;
    private bool musicLoop;
    private float musicSpetialBlend;

    
    void Awake()
    {
        if (menuMusic == null)
        {
            DontDestroyOnLoad(gameObject);
            menuMusic = this;
            
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            musicPitch = 1.0f;
            musicLoop = true;
            musicSpetialBlend = 0.0f;
        }
        else if (menuMusic != this)
        {
            Destroy(gameObject);
        }
    }


	void Start () {
		
        if(menuMusicClip != null)
        {
            audioSource.clip = menuMusicClip;
        }

        Play();

	}
	


    void SetSourceProperties(AudioClip clip, float volume, float pitch, bool loop, float spatialBlend)
    {
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.pitch = pitch;
        audioSource.loop = loop;
        audioSource.spatialBlend = spatialBlend;
    }


    public void Play()
    {
     
            SetSourceProperties(menuMusicClip, ((GameData.gameData.musicVol / 100.0f) * (GameData.gameData.masterVol / 100.0f)), musicPitch, musicLoop, musicSpetialBlend);

            audioSource.Play();
        
    }

    public void UpdateVolume(float volume)
    {
        audioSource.volume = volume;
    }

    public void StopMusic()
    {
        audioSource.Stop();
    }

}
