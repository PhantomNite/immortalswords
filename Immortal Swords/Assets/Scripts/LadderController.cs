﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LadderController : MonoBehaviour {

    public static LadderController ladderControl;

    //set difficulty
    //set sword
    //ladder position count
    //reset next  fight

    public GameObject ladderMan;
    public GameObject aiManager;
    public GameObject winMenu;
    public GameObject continueMenu;
    public GameObject retryMenu;
    private AIManager aiScript;

    private int ladderPosition;
    private int ladderMax;
    private int difficulty;
    private int swordID;

    //continue menu variables
    public GameObject continueCursor;
    public GameObject menuCusor;
    private GameObject[] menuCursors;

    public GameObject retryCursor;
    public GameObject swordCusor;
    private GameObject[] retryCursors;


    private int optionNum;
    private float inputDelayTimer;
    private float inputDelay;

    // Use this for initialization
    void Start () {
        ladderMax = 4;
        inputDelay = 0.25f;

        ladderPosition = GameData.gameData.ladderPos;

        continueCursor.SetActive(true);
        menuCusor.SetActive(false);
        menuCursors = new GameObject[2];
        menuCursors[0] = continueCursor;
        menuCursors[1] = menuCusor;

        retryCursor.SetActive(true);
        swordCusor.SetActive(false);
        retryCursors = new GameObject[2];
        retryCursors[0] = retryCursor;
        retryCursors[1] = swordCusor;

        aiScript = aiManager.GetComponent<AIManager>();
        aiScript.difficulty = difficulty = GameData.gameData.aiDifficulty;
        aiScript.swordID = swordID = GameData.gameData.aiSwordID;

    }
	
	// Update is called once per frame
	void Update () {

        if (winMenu.activeInHierarchy)
        {
            if (Input.GetButton("A_1") || Input.GetKeyDown(KeyCode.Return)) //A
            {
                GameData.gameData.ladderPos = 0;
                GameData.gameData.aiDifficulty = 0;
                SceneManager.LoadScene("SwordSelect");
            }
        }
        else if (continueMenu.activeInHierarchy)
        {
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //enter key pressed
            {
                if (optionNum == 0)
                {
                    GameData.gameData.ladderPos = ladderPosition;
                    GameData.gameData.aiDifficulty = difficulty;
                    GameData.gameData.aiSwordID = swordID;
                    continueMenu.SetActive(false);
                    SceneManager.LoadScene("Stage");
                }
                else
                {
                    GameData.gameData.ladderPos = 0;
                    GameData.gameData.aiDifficulty = 0;
                    SceneManager.LoadScene("MainMenu");
                }
            }
            else if ((Input.GetAxis("L_YAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //up key
            {
                if (optionNum > 0)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum--;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
            {
                if (optionNum < menuCursors.Length - 1)
                {
                    menuCursors[optionNum].SetActive(false);
                    optionNum++;
                    menuCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
        }
        else if (retryMenu.activeInHierarchy)
        {
            if (Input.GetButtonDown("A_1") || Input.GetKeyDown(KeyCode.Return)) //enter key pressed
            {
                if (optionNum == 0)
                {
                    ladderPosition = 0;
                    aiScript.difficulty = difficulty = 0;
                    aiScript.swordID = swordID = GameData.gameData.aiSwordID;
                    retryMenu.SetActive(false);
                    SceneManager.LoadScene("Stage");
                }
                else
                {
                    GameData.gameData.ladderPos = 0;
                    GameData.gameData.aiDifficulty = 0;
                    SceneManager.LoadScene("SwordSelect");
                }
            }
            else if ((Input.GetAxis("L_YAxis_1") < -0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.W)) //up key
            {
                if (optionNum > 0)
                {
                    retryCursors[optionNum].SetActive(false);
                    optionNum--;
                    retryCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
            else if ((Input.GetAxis("L_YAxis_1") > 0.5f && inputDelayTimer >= inputDelay) || Input.GetKeyDown(KeyCode.S)) //Down key pressed
            {
                if (optionNum < menuCursors.Length - 1)
                {
                    retryCursors[optionNum].SetActive(false);
                    optionNum++;
                    retryCursors[optionNum].SetActive(true);
                }
                inputDelayTimer = 0.0f;
            }
        }

    }

    public void updateLadder()
    {

        continueMenu.SetActive(true);
        ladderPosition++;
        if (ladderPosition >= ladderMax)
        {
            //beat last opponent
            GameData.gameData.ladderPos = 0;
            //show win screen
            continueMenu.SetActive(false);
            winMenu.SetActive(true);

            //take back to weapon select
        }

        //restart state with next AI
        if (difficulty < 2) {
            difficulty++;
        }
        swordID++;

    }




}
