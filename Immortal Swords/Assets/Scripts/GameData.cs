﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameData : MonoBehaviour {

    public static GameData gameData;

    public enum Menus { Main, Option, SinglePlayer, Versus, Ladder, Training, Tutorial };
    public bool loadedOptions;
    public bool LoadedPlayer;
    public string[] controler1Input;
    public KeyCode[] keyboardInput;

    //Player Data vars
    public int wins;
    public bool[] unlocks;

    //Option Data vars
    public string windowSize;
    public bool fullscreen;
    public float brightness;
    public float masterVol;
    public float musicVol;
    public float effectsVol;

    //ladder vars
    public int aiDifficulty;
    public int aiSwordID;
    public int ladderPos;


    //Temp Game Data vars
    public Menus menuType;
    public bool aiVersus;
    public int roundNum;
    public int player1SwordID;
    public int player2SwordID;

	
	void Awake () {

        if (gameData == null)
        {
            DontDestroyOnLoad(gameObject);
            gameData = this;

            loadedOptions = false;
            LoadedPlayer = false;

            //initalize defualt player values
            wins = 0;
            unlocks = new bool[16];
            unlocks[0] = true;
            unlocks[1] = true;
            unlocks[2] = true;
            unlocks[3] = true;
            for(int i = 4; i < 16; i++)
            {
                unlocks[i] = false;
            }


            //initalize default option values
            windowSize = "1920 x 1080";
            fullscreen = false;
            brightness = 50.00f;
            masterVol = 50.00f;
            musicVol = 50.00f;
            effectsVol = 50.00f;


            //initalize default game values
            menuType = Menus.Main;
            aiVersus = false;
            aiDifficulty = 0;
            roundNum = 3;
            ladderPos = 0;

            LoadOptionData();
            LoadPlayerData();

            controler1Input = new string[] { "A_1", "B_1","X_1", "Y_1", "LB_1", "RB_2", "Start_1", "Back_1", "TriggersL_1", "TriggersL_1", "L_Xaxis_1", "L_YAxis_1", "R_XAxis_1", "R_YAxis_1", "DPad_XAxis_1", "DPad_YAxis_1", "LS_1", "RS_1" };
            keyboardInput = new KeyCode[] { KeyCode.K, KeyCode.L, KeyCode.O, KeyCode.P, KeyCode.Q, KeyCode.E, KeyCode.Return, KeyCode.Escape, KeyCode.Alpha1, KeyCode.Alpha3, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.S };


            


        }
        else if(gameData != this)
        {
            Destroy(gameObject);
        }

        
	}
	



    public void SavePlayerData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData pData = new PlayerData();
        pData.pData_wins = wins;
        for(int i = 0; i < 16; i++)
        {
            pData.pData_unlocks[i] = unlocks[i];
        }

        bf.Serialize(file, pData);
        file.Close();
    }

    public void LoadPlayerData()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData pData = (PlayerData)bf.Deserialize(file);
            file.Close();

            wins = pData.pData_wins;
            for (int i = 0; i < 16; i++)
            {
                unlocks[i] = pData.pData_unlocks[i];
            }

            LoadedPlayer = true;
        }
        else
        {
            LoadPlayerDataBkUp();
        }
    }
    public void SavePlayerDataBkUp()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfoBkUp.dat");

        PlayerData pData = new PlayerData();
        pData.pData_wins = wins;
        for (int i = 0; i < 16; i++)
        {
            pData.pData_unlocks[i] = unlocks[i];
        }

        bf.Serialize(file, pData);
        file.Close();
    }

    private void LoadPlayerDataBkUp()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfoBkUp.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData pData = (PlayerData)bf.Deserialize(file);
            file.Close();

            wins = pData.pData_wins;
            for (int i = 0; i < 16; i++)
            {
                unlocks[i] = pData.pData_unlocks[i];
            }

            LoadedPlayer = true;
        }
    }
    public void SaveOptionData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/optionInfo.dat");  //C:/Users/*UserName*/AppData/LocalLow/DefaultCompany/Immortal Swords

        OptionData oData = new OptionData();

        oData.oData_windowSize = windowSize;
        oData.oData_fullscreen = fullscreen;
        oData.oData_brightness = brightness;
        oData.oData_masterVol = masterVol;
        oData.oData_musicVol = musicVol;
        oData.oData_effectsVol = effectsVol;

        bf.Serialize(file, oData);
        file.Close();

    }
    public void LoadOptionData()
    {
        if (File.Exists(Application.persistentDataPath + "/optionInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/optionInfo.dat", FileMode.Open);
            OptionData oData = (OptionData)bf.Deserialize(file);
            file.Close();

            windowSize = oData.oData_windowSize;
            fullscreen = oData.oData_fullscreen;
            brightness = oData.oData_brightness;
            masterVol = oData.oData_masterVol;
            musicVol = oData.oData_musicVol;
            effectsVol = oData.oData_effectsVol;

            loadedOptions = true;
        }
    }

}

//write class to file
[Serializable]
class PlayerData
{
    public int pData_wins;
    public bool[] pData_unlocks;

}

[Serializable]
class OptionData
{
    public string oData_windowSize;
    public bool oData_fullscreen;
    public float oData_brightness;
    public float oData_masterVol;
    public float oData_musicVol;
    public float oData_effectsVol;
}
